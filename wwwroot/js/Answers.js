﻿$("#ButtonSalvar").click(function () {
    var respostas = [];
    var avaliacaoId = $(".avaliacaoId").val();

    $(".show .resposta").each(function () {
        var respostaModel = new Object();
        respostaModel.RespostaId = parseInt($(this).find(".resposta-id").val());

        if ($(this).find(".resposta-valor").val() == "")
            respostaModel.Valor = 0.0;
        else respostaModel.Valor = $(this).find(".resposta-valor").val();

        respostaModel.Observacao = $(this).find(".resposta-observacao").val();
        respostaModel.AvaliacaoId = parseInt($(this).find(".avaliacao-id").val());
        respostaModel.QuestaoId = parseInt($(this).find(".questao-id").val());
        respostaModel.QuestaoConteudo = $(this).find(".questao-conteudo").val();
        respostaModel.Avaliacao = null;
        respostaModel.Questao = null;
        respostas.push(respostaModel);
    });

    $.post("/Avaliacao/Responder", { AvaliacaoId: avaliacaoId, RespostasView: respostas }, function (data) {
        var item = '';
        if (data == true) {
            item = '<div class="alert alert-success alert-dismissible fade show" role="alert">';
            item += '<h4 class="alert-heading">Sucesso!</h4>';
            item += '<p>As respostas foram salvas com sucesso!</p>';
            item += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
            item += '<span aria-hidden="true">&times;</span>';
            item += '</button>';
            item += '</div>';
            $("#msg").html(item);
        }
        else {
            item = '<div class="alert alert-danger alert-dismissible fade show" role="alert">';
            item += '<h4 class="alert-heading">Erro!</h4>';
            item += '<p>As respostas não foram inseridas corretamente!</p>';
            item += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
            item += '<span aria-hidden="true">&times;</span>';
            item += '</button>';
            item += '</div>';
            $("#msg").html(item);
        }
    });
});