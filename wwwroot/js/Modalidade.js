﻿$(document).ready(function () {
    $("#pontos").maskMoney({ decimal: "," });
    $("#pontos").prop("disabled", getTipoModalidade());
});

$("#TipoModalidade").change(function () {
    $("#pontos").prop("disabled", getTipoModalidade());
});

function getTipoModalidade() {
    if ($("#TipoModalidade").val() == 'Pontuação p/ Hora' || $("#TipoModalidade").val() == 'Pontuação p/ Item')
        return true;
    return false;
}