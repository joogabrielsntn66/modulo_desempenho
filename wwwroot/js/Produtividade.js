﻿$(document).ready(function () {
    var auditorId = $("#AuditorId").val();
    var produtoId = $("#ProdutoId").val();
    var urlAuditor = "/Produtividades/GetUnidadeFromAuditor";
    var urlComplexidade = "/Produtividades/GetComplexidadesFromProduto";
    var urlProduto = "/Produtividades/GetProduto";

    getUnidadeFromAuditor(auditorId, urlAuditor);
    getComplexidadeFromProduto(produtoId, urlComplexidade);
    getProduto(produtoId, urlProduto);

    $("#Termino").prop("disabled", $("#Situacao").val() == 'Finalizado');
    $("#Conclusao").prop("disabled", $("#Situacao").val() == 'Em Elaboração');
    $("#NumeroDemanda").prop("disabled", $("#Expediente").val() == "Outros");
});


$("#AuditorId").change(function () {
    var auditorId = $("#AuditorId").val();
    var url = "/Produtividades/GetUnidadeFromAuditor";

    getUnidadeFromAuditor(auditorId, url);
});

$("#ProdutoId").change(function () {
    var produtoId = $("#ProdutoId").val();

    var url = "/Produtividades/GetComplexidadesFromProduto";
    var urlProduto = "/Produtividades/GetProduto";

    getComplexidadeFromProduto(produtoId, url);
    getProduto(produtoId, urlProduto);
});

$("#Situacao").change(function () {
    $("#Termino").prop("disabled", $(this).val() == 'Finalizado');
    $("#Conclusao").prop("disabled", $(this).val() == 'Em Elaboração');
});

$("#Expediente").change(function () {
    $("#NumeroDemanda").prop("disabled", $(this).val() == "Outros");
});

function getUnidadeFromAuditor(auditorId, url) {

    $.getJSON(url, { AuditorId: auditorId }, function (data) {
        var item = "<option disabled>Selecione uma Unidade aqui</option>";
        $("#Unidades").empty();
        $.each(data, function (i, unidade) {
            item += '<option value="' + unidade.value + '">' + unidade.text + '</option>'
        });
        $("#Unidades").html(item);
    });
    return;
}

function getComplexidadeFromProduto(produtoId, url) {
    $.getJSON(url, { ProdutoId: produtoId }, function (data) {
        var item = "<option disabled>Selecione uma Complexidade aqui</option>";
        $("#Complexidades").empty();
        $.each(data, function (i, complexidade) {
            item += '<option value="' + complexidade.value + '">' + complexidade.text + '</option>'
        });
        $("#Complexidades").html(item);
    });
    return;
}

function getProduto(produtoId, url) {
    $.getJSON(url, { ProdutoId: produtoId }, function (produto) {
        $("#TipoPontuacao").val(produto[0].tipoPontuacao);

        if (produto[0].tipoPontuacao == "Fixa") {
            $("#HorasDispendidas").prop("disabled", true);
        }
        else if (produto[0].tipoPontuacao == "Variável") {
            $("#HorasDispendidas").prop("disabled", false);
        }

        $("#PontoBase").val(produto[0].pontoBase);
    });
    return;
}