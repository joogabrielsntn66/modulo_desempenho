﻿$(document).ready(function () {
    var modalidadeId = $("#ModalidadeId").val();
    var urlRequisito = "/Capacitacoes/GetRequisitosFromModalidade";
    var nomeTema = $("#Temas").val();
    var urlTema = "/Capacitacoes/GetTema";
    var urlModalidade = "/Capacitacoes/GetModalidade";

    getRequisitoFromModalidade(modalidadeId, urlRequisito);
    getTema(nomeTema, urlTema);
    getModalidade(modalidadeId, urlModalidade);
});

$("#ModalidadeId").change(function () {
    var modalidadeId = $("#ModalidadeId").val();
    var url = "/Capacitacoes/GetRequisitosFromModalidade";
    var urlModalidade = "/Capacitacoes/GetModalidade";

    getRequisitoFromModalidade(modalidadeId, url);
    getModalidade(modalidadeId, urlModalidade);
});

$("#EixoId").change(function () {
    var eixoId = $("#EixoId").val();
    var url = "/Capacitacoes/GetTemasFromEixo";

    getTemaFromEixo(eixoId, url);
});

$("#EixoId").click(function () {
    var eixoId = $("#EixoId").val();
    var url = "/Capacitacoes/GetTemasFromEixo";

    getTemaFromEixo(eixoId, url);
});

$("#Temas").click(function () {
    var nomeTema = $("#Temas").val();
    var url = "/Capacitacoes/GetTema";

    getTema(nomeTema, url);
});

function getRequisitoFromModalidade(modalidadeId, url) {
    $.getJSON(url, { ModalidadeId: modalidadeId }, function (data) {
        var item = "<option disabled>Selecione um Requisito aqui</option>";
        $("#Requisitos").empty();
        $.each(data, function (i, requisito) {
            item += '<option value="' + requisito.value + '">' + requisito.text + '</option>'
        });
        $("#Requisitos").html(item);
    });
    return;
}

function getTemaFromEixo(eixoId, url) {
    $.getJSON(url, { EixoId: eixoId }, function (data) {
        var item = "<option disabled>Selecione um Tema aqui</option>";
        $("#Temas").empty();
        $.each(data, function (i, eixo) {
            item += '<option value="' + eixo.value + '">' + eixo.text + '</option>'
        });
        $("#Temas").html(item);
    });
    return;
}

function getTema(nomeTema, url) {
    $.getJSON(url, { NomeTema: nomeTema }, function (tema) {
        $("#Relevancia").val(tema[0]);
    });
    return;
}

function getModalidade(modalidadeId, url) {
    $.getJSON(url, { ModalidadeId: modalidadeId }, function (modalidade) {
        if (modalidade[0] == "Pontuação Fixa") {
            $("#Requisitos").prop("disabled", true);
            $("#EixoId").prop("disabled", true);
            $("#Temas").prop("disabled", true);
        }
        else if (modalidade[0] == "Pontuação p/ Hora" || modalidade[0] == "Pontuação p/ Item") {
            $("#EixoId").prop("disabled", true);
            $("#Temas").prop("disabled", true);
            $("#Requisitos").prop("disabled", false);
        }
        else {
            $("#Requisitos").prop("disabled", false);
            $("#EixoId").prop("disabled", false);
            $("#Temas").prop("disabled", false);
        }
    });
    return;
}