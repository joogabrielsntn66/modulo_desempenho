﻿// Scripts para funcionamento do Relatório Auditor/Produto

$("#AddAuditor").click(function () {
    var auditorSelecionado = $("#Auditores").find("option:selected").text();
    var auditorId = $("#Auditores").find("option:selected").val();

    if (auditorSelecionado != null) {
        $('#Auditores option[value="' + auditorId + '"]').attr("disabled", true);
        var item = criarItem("auditor", auditorId, auditorSelecionado);
        $("#AuditoresSelecionados").append(item);
    }
});

$("#AddProduto").click(function () {
    var produtoSelecionado = $("#Produtos").find("option:selected").text();
    var produtoId = $("#Produtos").find("option:selected").val();

    if (produtoSelecionado != null) {
        $('#Produtos option[value="' + produtoId + '"]').attr("disabled", true);
        var item = criarItem("produto", produtoId, produtoSelecionado);
        $("#ProdutosSelecionados").append(item);
    }
});

$("#ButtonLimpar").click(function () {
    $('#Auditores option').attr("disabled", false);
    $("#AuditoresSelecionados").empty();
    $('#Produtos option').attr("disabled", false);
    $("#ProdutosSelecionados").empty();
});

$("#AuditoresSelecionados").on('click', '.auditorDelete', function (e) {
    var item = this;
    deleteItem(e, item);
    $('#Auditores option').attr("disabled", false);
});

$("#ProdutosSelecionados").on('click', '.produtoDelete', function (e) {
    var item = this;
    deleteItem(e, item);
    $('#Produtos option').attr("disabled", false);
});

$("#ButtonGerar").click(function () {
    var auditores = [], produtos = [], situacao = '';

    $(".auditorId").each(function () {
        var auditor = parseInt($(this).val());
        auditores.push(auditor);
    });
    $(".produtoId").each(function () {
        var produto = parseInt($(this).val());
        produtos.push(produto);
    });
    situacao = $("#Situacao").val();

    pegarJSON(auditores, produtos, situacao);
});

function pegarJSON(auditores, produtos, situacao) {
    $.getJSON("dados", $.param({
        auditoresView: auditores,
        produtosView: produtos,
        Situacao: situacao
    }, true), function (data) {
        chamarTable(data);
    });
    return;
}