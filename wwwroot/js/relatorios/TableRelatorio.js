﻿function chamarTable(data) {
    var dados = [], colunas = [];

    $.each(data, function (i) {
        if (colunas.length == 0) {
            Object.getOwnPropertyNames(data[i]).forEach(function (val) {
                colunas.push({ "field": val, "title": val, sortable: true });
            });
        }
    });

    dados = data;

    if ($("#table").is(":empty") == false)
        $("#table").bootstrapTable('destroy');

    mostrarBootTable(dados, colunas);
}

// Renderiza a Table com os dados e as colunas respectivas
function mostrarBootTable(dados, colunas) {
    $('#table').bootstrapTable({
        data: dados,
        pagination: true,
        search: true,
        locale: "pt-BR",
        columns: colunas,
    });
}

// Cria um elemento <li> para filtragem de dados do relatório
function criarItem(entidade, entidadeValue, entidadeText) {
    var item = '<li class="list-group-item">';
    item += '<div class="row justify-content-between"><div class="col">';
    item += '<input class="' + entidade + 'Id" type="hidden" value="' + entidadeValue + '">';
    item += '<input class="form-control-plaintext ' + entidade + '" value="' + entidadeText + '" readonly>';
    item += '</div><div class="col-md-auto">';
    item += '<button class="btn btn-sm btn-danger ' + entidade + 'Delete"><i class="fas fa-trash-alt"></i></button>';
    item += '</div></div></li>';
    return item;
}

// Função acionada para deletar um elemento <li>
function deleteItem(e, item) {
    e.preventDefault();
    $(item).parentsUntil("ul").fadeOut('slow', function () {
        $(item).parentsUntil("ul").remove();
    });
    return;
}
