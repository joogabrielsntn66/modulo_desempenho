﻿
$("#AddAuditor").click(function () {
    var auditorSelecionado = $("#Auditores").find("option:selected").text();
    var auditorId = $("#Auditores").find("option:selected").val();

    if (auditorSelecionado != null) {
        $('#Auditores option[value="' + auditorId + '"]').attr("disabled", true);
        var item = criarItem("auditor", auditorId, auditorSelecionado);
        $("#AuditoresSelecionados").append(item);
    }
});

$("#AuditoresSelecionados").on('click', '.auditorDelete', function (e) {
    var item = this;
    deleteItem(e, item);
    $('#Auditores option').attr("disabled", false);
});

$("#ButtonLimpar").click(function () {
    $('#Auditores option').attr("disabled", false);
    $("#AuditoresSelecionados").empty();
});

$("#ButtonGerar").click(function () {
    var auditores = [], situacao = '';

    $(".auditorId").each(function () {
        var auditor = parseInt($(this).val());
        auditores.push(auditor);
    });
    situacao = $("#Situacao").val();

    pegarJSON(auditores, situacao);
});

function pegarJSON(auditores, situacao) {
    $.getJSON("dados_certos", $.param({
        auditoresView: auditores,
        Situacao: situacao
    }, true), function (data) {
        chamarTable(data);
    });
    return;
}