﻿// Scripts para funcionamento do Relatório Produto/Produtividade

$("#AddProduto").click(function () {
    var produtoSelecionado = $("#Produtos").find("option:selected").text();
    var produtoId = $("#Produtos").find("option:selected").val();

    if (produtoSelecionado != null) {
        $('#Produtos option[value="' + produtoId + '"]').attr("disabled", true);
        var item = criarItem("produto", produtoId, produtoSelecionado);
        $("#ProdutosSelecionados").append(item);
    }
});

$("#ButtonLimpar").click(function () {
    $('#Produtos option').attr("disabled", false);
    $("#ProdutosSelecionados").empty();
});

$("#ProdutosSelecionados").on('click', '.produtoDelete', function (e) {
    var item = this;
    deleteItem(e, item);
    $('#Produtos option').attr("disabled", false);
});

$("#ButtonGerar").click(function () {
    var produtos = [], situacao = '';

    $(".produtoId").each(function () {
        var produto = parseInt($(this).val());
        produtos.push(produto);
    });
    situacao = $("#Situacao").val();

    pegarJSON(produtos, situacao);
});

function pegarJSON(produtos, situacao) {
    $.getJSON("dados", $.param({
        produtosView: produtos,
        Situacao: situacao
    }, true), function (data) {
        chamarTable(data);
    });
    return;
}
