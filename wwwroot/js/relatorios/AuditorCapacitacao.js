﻿$("#AddAuditor").click(function () {
    var auditorSelecionado = $("#Auditores").find("option:selected").text();
    var auditorId = $("#Auditores").find("option:selected").val();

    if (auditorSelecionado != null) {
        $('#Auditores option[value="' + auditorId + '"]').attr("disabled", true);
        var item = criarItem("auditor", auditorId, auditorSelecionado);
        $("#AuditoresSelecionados").append(item);
    }
});

$("#AuditoresSelecionados").on('click', '.auditorDelete', function (e) {
    var item = this;
    deleteItem(e, item);
    $('#Auditores option').attr("disabled", false);
});

$("#ButtonLimpar").click(function () {
    $('#Auditores option').attr("disabled", false);
    $("#AuditoresSelecionados").empty();
});

$("#ButtonGerar").click(function () {
    var auditores = [], situacao = '';

    $(".auditorId").each(function () {
        var auditor = parseInt($(this).val());
        auditores.push(auditor);
    });
    situacao = $("#Situacao").val();

    pegarJSON(auditores, situacao);
});

function pegarJSON(auditores, situacao) {
    $.getJSON("dados", $.param({
        auditoresView: auditores,
        Situacao: situacao
    }, true), function (data) {
        chamarTableCapacitacao(data);
    });
    return;
} 

function chamarTableCapacitacao(data) {
    var dados = [], headers = [];
    // Colunas de baixo
    var colunas = [];
    // Colunas de cima
    headers = data[0];
    // As linhas da tabelas
    dados = data[1];

    $.each(dados, function (i) {
        if (colunas.length == 0) {
            Object.getOwnPropertyNames(dados[i]).forEach(function (val) {
                if (val.includes("/")) {
                    var titulo = val.split("/");
                    colunas.push({ "field": val, "title": titulo[1], sortable: true });
                }
                else
                    colunas.push({ "field": val, "title": val, sortable: true });
            });
        }
    });

    // Para as colunas de cima e de baixo acoplarem-se,
    // precisamos excluir o primeiro elemento das colunas de baixo
    colunas.shift();
    // precisamos também excluir as duas últimas das colunas de baixo
    colunas.splice(-2);

    // Se a tabela já estiver, temos que destruí-la
    if ($("#table").is(":empty") == false)
        $("#table").bootstrapTable('destroy');

    // Aqui, de fato, renderizamos a tabela
    $('#table').bootstrapTable({
        data: dados,
        pagination: true,
        search: true,
        columns: [
            headers,
            colunas,
        ]
    });

    // Espero que dê certo
    return;
}