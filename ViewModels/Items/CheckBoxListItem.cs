﻿using System;
using System.Collections.Generic;
namespace desempenho_cge.ViewModels.Items
{
    public class CheckBoxListItem
    {
        public int Id { get; set; }
        public string Display { get; set; }
        public bool IsChecked { get; set; }
    }
}
