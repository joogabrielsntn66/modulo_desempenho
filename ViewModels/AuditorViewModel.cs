﻿using System.ComponentModel.DataAnnotations;

namespace desempenho_cge.ViewModels
{
    public class AuditorViewModel
    {
        [Display(Name = "Identificador")]
        public int AuditorId { get; set; }
        [Display(Name = "Matrícula")]
        [Required(ErrorMessage = "O campo Matrícula não pode ficar vazio")]
        public string Matricula { get; set; }
        [Display(Name = "Nome do Auditor(a)")]
        [Required(ErrorMessage = "O campo Nome não pode ficar vazio")]
        public string Nome { get; set; }
        [Display(Name = "Selecione a unidade")]
        [Required(ErrorMessage = "É necessário selecionar uma unidade")]
        public int UnidadeId { get; set; }
        public virtual UnidadeViewModel Unidade { get; set; }
    }
}
