﻿using System;
using System.ComponentModel.DataAnnotations;

namespace desempenho_cge.ViewModels
{
    public class OrgaoViewModel
    {
        [Display(Name = "Identificador")]
        public int OrgaoId { get; set; }
        [Display(Name = "Unidade Orçamentária")]
        [Required(ErrorMessage = "O campo Unidade Orçamentária não pode ficar vazio")]
        public string Nome { get; set; }
        [Display(Name = "Sigla")]
        [Required(ErrorMessage = "O campo Sigla não pode ficar vazio")]
        public string Sigla { get; set; }
    }
}
