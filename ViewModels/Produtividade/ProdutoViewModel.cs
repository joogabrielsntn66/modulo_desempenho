﻿using desempenho_cge.ViewModels.Items;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace desempenho_cge.ViewModels.Produtividade
{
    public class ProdutoViewModel
    {
        [Display(Name = "Identificador")]
        public int ProdutoId { get; set; }
        [Display(Name = "Nome do Produto")]
        [Required(ErrorMessage = "O campo Nome não pode ficar vazio")]
        public string Nome { get; set; }
        [Display(Name = "Esforço (Auditor p/ hora)")]
        [Required(ErrorMessage = "O campo Esforço não pode ficar vazio")]
        public int Esforco { get; set; }
        [Display(Name = "Pontos Base")]
        public double PontoBase { get; set; }
        [Display(Name = "Tipo de Pontuação")]
        public string TipoPontuacao { get; set; }
        public ICollection<CheckBoxListItem> Complexidades { get; set; }

        public ProdutoViewModel()
        {
            Complexidades = new List<CheckBoxListItem>();
        }
    }
}
