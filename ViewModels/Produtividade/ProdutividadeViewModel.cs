﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace desempenho_cge.ViewModels.Produtividade
{
    public class ProdutividadeViewModel
    {
        [Display(Name = "Identificador")]
        public int ProdutividadeId { get; set; }
        [Required(ErrorMessage = "É necessário selecionar um Auditor")]
        public int AuditorId { get; set; }
        public virtual AuditorViewModel Auditor { get; set; }
        [Required(ErrorMessage = "É necessário selecionar um Produto")]
        public int ProdutoId { get; set; }
        public virtual ProdutoViewModel Produto { get; set; }
        [Required(ErrorMessage = "É necessário selecionar uma Unidade Orçamentária")]
        public int OrgaoId { get; set; }
        public virtual OrgaoViewModel Orgao { get; set; }
        [Display(Name = "Se outros, detalhar:")]
        public string UnidadeOrcamentaria { get; set; }
        [Display(Name = "Unidade de Execução:")]
        [Required(ErrorMessage = "É necessário selecionar uma Unidade de Execução")]
        public string UnidadeExecucao { get; set; }
        [Display(Name = "Complexidade:")]
        [Required(ErrorMessage = "É necessário selecionar uma Complexidade")]
        public string Complexidade { get; set; }
        [Display(Name = "Horas dispendidas:")]
        public int? HorasDipendidas { get; set; }
        [Display(Name = "Trabalho a ser desenvolvido:")]
        [Required(ErrorMessage = "É necessário descrever a natureza do trabalho")]
        public string Assunto { get; set; }
        [Display(Name = "Número O.S:")]
        public string NumeroOrdem { get; set; }
        [Display(Name = "Ano O.S:")]
        public string AnoOrdem { get; set; }
        [Display(Name = "Prazo(dias):")]
        public int Prazo { get; set; }
        [Display(Name = "Situação:")]
        public string Situacao { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Início:")]
        [Required(ErrorMessage = "Selecione uma data de Início")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DataInicio { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Término(Previsto):")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? DataTermino { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Data de Conclusão:")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? DataConclusao { get; set; }
        [Display(Name = "Nº do Produto:")]
        public string NumeroProduto { get; set; }
        [Display(Name = "Ano do Produto:")]
        public string AnoProduto { get; set; }
        [Display(Name = "Expediente Originário:")]
        [Required(ErrorMessage = "É necessário especificar a Origem da Demanda")]
        public string ExpedienteOriginario { get; set; }
        [Display(Name = "Número:")]
        public string NumeroDemanda { get; set; }
        [Display(Name = "Ano:")]
        public string AnoDemanda { get; set; }
        [Display(Name = "Se outros, detalhar: ")]
        public string ObservacoesDemanda { get; set; }
        [Display(Name = "Observações Gerais:")]
        public string ObservacoesGerais { get; set; }
        [Display(Name = "Pontuação Atribuída:")]
        public double Pontuacao { get; set; }
    }
}
