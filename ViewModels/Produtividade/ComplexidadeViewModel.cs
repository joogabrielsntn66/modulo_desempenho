﻿using System.ComponentModel.DataAnnotations;

namespace desempenho_cge.ViewModels.Produtividade
{
    public class ComplexidadeViewModel
    {
        [Display(Name = "Identificador")]
        public int ComplexidadeId { get; set; }
        [Display(Name = "Classificação")]
        [Required(ErrorMessage = "O campo Classificação não pode ficar vazio")]
        public string Classificacao { get; set; }
        [Display(Name = "Peso")]
        [Required(ErrorMessage = "O campo Peso não pode ficar vazio")]
        public double Peso { get; set; }
    }
}
