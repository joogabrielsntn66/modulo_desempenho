﻿using System.Collections.Generic;

namespace desempenho_cge.ViewModels.Qualidade
{
    public class AvaliacaoRespostasViewModel
    {
        public int AvaliacaoId { get; set; }
        public List<GrupoRespostasViewModel> Grupos { get; set; }
    }
}
