﻿using desempenho_cge.Models;
using desempenho_cge.Models.Account;
using desempenho_cge.Models.Produtividade;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace desempenho_cge.ViewModels
{
    public class AvaliacaoViewModel
    {
        public int AvaliacaoId { get; set; }
        [Required(ErrorMessage = "É necessário selecionar um Produto")]
        [Display(Name = "Produto:")]
        public int ProdutoId { get; set; }
        public virtual Produto Produto { get; set; }
        [Display(Name = "Número:")]
        public string NumeroProduto { get; set; }
        [Display(Name = "Ano:")]
        public string AnoProduto { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Data de Entrega:")]
        [Required(ErrorMessage = "Selecione uma data de entrega")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DataEntrega { get; set; }
        [Display(Name = "Número:")]
        public string NumeroServico { get; set; }
        [Display(Name = "Ano:")]
        public string AnoServico { get; set; }
        [Required(ErrorMessage = "É necessário selecionar um Auditor(a)")]
        [Display(Name = "Auditor(a):")]
        public int AuditorId { get; set; }
        public virtual Auditor Auditor { get; set; }
        [Required(ErrorMessage = "É necessário selecionar uma unidade de execução")]
        [Display(Name = "Unidade de Execução:")]
        public string UnidadeExecucao { get; set; }
        [Required(ErrorMessage = "É necessário selecionar um Questionário")]
        [Display(Name = "Questionário:")]
        public int QuestionarioId { get; set; }
        public virtual Questionario Questionario { get; set; }
        public virtual ICollection<Resposta> Respostas { get; set; }
    }
}
