﻿using desempenho_cge.Models;
using System.Collections.Generic;

namespace desempenho_cge.ViewModels.Qualidade
{
    public class GrupoRespostasViewModel
    {
        public int GrupoId { get; set; }
        public string Conteudo { get; set; }
        public List<Resposta> Respostas { get; set; }

        public GrupoRespostasViewModel()
        {
            Respostas = new List<Resposta>();
        }
    }
}
