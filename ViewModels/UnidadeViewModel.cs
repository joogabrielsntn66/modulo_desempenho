﻿using System.ComponentModel.DataAnnotations;

namespace desempenho_cge.ViewModels
{
    public class UnidadeViewModel
    {
        [Display(Name = "Identificador")]
        public int UnidadeId { get; set; }
        [Display(Name = "Nome da Unidade")]
        [Required(ErrorMessage = "O campo Nome não pode ficar vazio")]
        public string Nome { get; set; }
        [Display(Name = "Sigla da Unidade")]
        [Required(ErrorMessage = "O campo Sigla não pode ficar vazio")]
        public string Sigla { get; set; }
    }
}
