﻿using System.ComponentModel.DataAnnotations;

namespace desempenho_cge.ViewModels
{
    public class TemaViewModel
    {
        [Display(Name = "Identificador")]
        public int TemaId { get; set; }

        [Required(ErrorMessage = "O campo Nome não pode ficar vazio")]
        [Display(Name = "Nome do Tema")]
        public string Nome { get; set; }

        [Display(Name = "Grau de Relevância")]
        public string Relevancia { get; set; }

        [Display(Name = "Eixo de Conhecimento")]
        public int EixoId { get; set; }
        public virtual EixoViewModel Eixo { get; set; }
    }
}
