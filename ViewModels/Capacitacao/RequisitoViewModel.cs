﻿using System.ComponentModel.DataAnnotations;

namespace desempenho_cge.ViewModels
{
    public class RequisitoViewModel
    {
        [Display(Name = "Identificador")]
        public int RequisitoId { get; set; }
        [Required(ErrorMessage = "O nome do requisito não pode ser vazio")]
        [Display(Name = "Nome do Requisito")]
        public string Nome { get; set; }
        [Required(ErrorMessage = "O campo Pontos não pode ficar vazio")]
        public double Pontos { get; set; }
        [Display(Name = "Escolha a Modalidade")]
        public int ModalidadeId { get; set; }
        public virtual ModalidadeViewModel Modalidade { get; set; }
    }
}
