﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace desempenho_cge.ViewModels
{
    public class ModalidadeViewModel
    {
        [Display(Name = "Identificador")]
        public int ModalidadeId { get; set; }
        [Required(ErrorMessage = "O nome da modalidade não pode ser vazio")]
        [Display(Name = "Nome da Modalidade")]
        public string Nome { get; set; }
        [Display(Name = "Tipo de Modalidade")]
        [Required(ErrorMessage = "O campo Tipo de Modalidade não pode ficar vazio")]
        public string TipoModalidade { get; set; }
        [Display(Name = "Pontos Base")]
        public double? Pontos { get; set; }
        public ICollection<RequisitoViewModel> Requisitos { get; set; }
    }
}
