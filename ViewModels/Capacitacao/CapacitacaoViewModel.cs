﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;

namespace desempenho_cge.ViewModels
{
    public class CapacitacaoViewModel
    {
        public int CapacitacaoId { get; set; }
        [Display(Name = "Eixo de Conhecimento")]
        public int? EixoId { get; set; }
        public virtual EixoViewModel Eixo { get; set; }
        public string Tema { get; set; }
        [Display(Name = "Modalidade")]
        public int ModalidadeId { get; set; }
        public virtual ModalidadeViewModel Modalidade { get; set; }
        public string Requisito { get; set; }
        [Display(Name = "Nome do Auditor")]
        public int AuditorId { get; set; }
        public virtual AuditorViewModel Auditor { get; set; }
        [Display(Name = "Nome do Curso/Evento")]
        [Required(ErrorMessage = "O campo Nome não pode ficar vazio")]
        public string NomeCurso { get; set; }
        [Display(Name = "Início")]
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Selecione uma data de início")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DataInicio { get; set; }
        [Display(Name = "Fim")]
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Selecione uma data de término")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DataFim { get; set; }
        public int Dias { get; set; }
        [Display(Name = "Carga Horária")]
        [Required(ErrorMessage = "O campo Carga Horária não pode ficar vazio")]
        public int CargaHoraria { get; set; }
        [Display(Name = "Grau de Relevância")]
        public string GrauRelevancia { get; set; }
        [Display(Name = "Instituição Promotora")]
        [Required(ErrorMessage = "O campo Instituição Promotora não pode ficar vazio")]
        public string InstituicaoPromotora { get; set; }
        [Display(Name = "Forma de Capacitação")]
        public string FormaCapacitacao { get; set; }
        [Display(Name = "Observações")]
        public string Observacao { get; set; }
        public string Homologado { get; set; }
        public IFormFile Documento { get; set; }
    }
}
