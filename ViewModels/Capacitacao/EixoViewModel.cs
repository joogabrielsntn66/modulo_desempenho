﻿using System.ComponentModel.DataAnnotations;

namespace desempenho_cge.ViewModels
{
    public class EixoViewModel
    {
        [Display(Name = "Identificador")]
        public int EixoId { get; set; }

        [Display(Name = "Nome do Eixo")]
        [Required(ErrorMessage = "O campo Nome não pode ficar vazio")]
        public string Nome { get; set; }
    }
}
