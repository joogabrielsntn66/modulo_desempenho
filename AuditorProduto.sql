﻿select 
	[Produtividades].[AuditorId] as auditor, 
	[Produtividades].[ProdutoId],
	SUM([Produtividades].[Pontuacao]) as pontuacao
from 
	[Produtividades]
group by 
	[Produtividades].[AuditorId], [Produtividades].[ProdutoId];