using desempenho_cge.Context;
using desempenho_cge.Models.Account;
using desempenho_cge.Repositories;
using desempenho_cge.Util.IdentityPolicy;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace desempenho_cge
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();

            // Produtividade
            services.AddScoped<ProdutividadeRepository>();
            services.AddScoped<ComplexidadeRepository>();
            services.AddScoped<ProdutoRepository>();

            // Sistema
            services.AddScoped<UnidadeRepository>();
            services.AddScoped<AuditorRepository>();
            services.AddScoped<OrgaoRepository>();

            // Capacitação
            services.AddScoped<CapacitacaoRepository>();
            services.AddScoped<EixoRepository>();
            services.AddScoped<TemaRepository>();
            services.AddScoped<ModalidadeRepository>();
            services.AddScoped<RequisitoRepository>();
            
            // Qualidade
            services.AddScoped<QuestionarioRepository>();
            services.AddScoped<QuestaoRepository>();
            services.AddScoped<AvaliacaoRepository>();
            services.AddScoped<RespostaRepository>();

            services.AddTransient<IPasswordValidator<AppUser>, CustomPasswordPolicy>();

            services.AddIdentity<AppUser, IdentityRole>(x => {
                x.User.RequireUniqueEmail = true;
                x.Password.RequiredLength = 6;
                x.Password.RequireDigit = false;
                x.Password.RequireLowercase = false;
                x.Password.RequireUppercase = false;
                x.Password.RequireNonAlphanumeric = true;
            })
                .AddEntityFrameworkStores<DefaultContext>()
                .AddDefaultTokenProviders();

            services.AddDbContext<DefaultContext>(options =>
             options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
