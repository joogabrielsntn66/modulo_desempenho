﻿using desempenho_cge.Context;
using desempenho_cge.Models.Produtividade;
using desempenho_cge.Util;
using desempenho_cge.ViewModels.Produtividade;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace desempenho_cge.Repositories
{
    public class ComplexidadeRepository
    {
        private readonly DefaultContext context;

        public ComplexidadeRepository(DefaultContext _context)
        {
            context = _context;
        }

        public Complexidade GetById(int id)
        {
            if(id > 0)
            {
                var complexidade = context.Complexidades
                    .AsNoTracking()
                    .Where(x => x.ComplexidadeId == id)
                    .Single();

                return complexidade;
            }

            return null;
        }

        public List<Complexidade> GetAll()
        {
            return context.Complexidades
                .AsNoTracking()
                .OrderBy(x => x.Peso)
                .ToList();
        }

        public void Save(Complexidade complexidade)
        {
            if(complexidade.ComplexidadeId == 0)
            {
                context.Complexidades.Add(complexidade);
            }
            else
            {
                context.Attach(complexidade).State = EntityState.Modified;
                context.Update(complexidade);
            }

            context.SaveChanges();
        }

        public void Delete(int id)
        {
            var complexidade = context.Complexidades.Find(id);

            if(complexidade != null)
            {
                context.Complexidades.Remove(complexidade);
                context.SaveChanges();
            }
        }

        public bool ComplexidadeExist(int id)
        {
            return context.Complexidades
                .Where(x => x.ComplexidadeId == id)
                .Any();
        }

        public ComplexidadeViewModel Converter(Complexidade complexidade)
        {
            var complexidadeView = new ComplexidadeViewModel();
            PropertyCopier<Complexidade, ComplexidadeViewModel>
                .Copy(complexidade, complexidadeView);
            return complexidadeView;
        }

        public Complexidade Converter(ComplexidadeViewModel complexidadeView)
        {
            var complexidade = new Complexidade();
            PropertyCopier<ComplexidadeViewModel, Complexidade>
                .Copy(complexidadeView, complexidade);
            return complexidade;
        }
    }
}
