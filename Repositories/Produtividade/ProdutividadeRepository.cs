﻿using desempenho_cge.Context;
using desempenho_cge.Models;
using desempenho_cge.Models.Produtividade;
using desempenho_cge.ViewModels;
using desempenho_cge.ViewModels.Produtividade;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace desempenho_cge.Repositories
{
    public class ProdutividadeRepository
    {
        private readonly DefaultContext context;

        public ProdutividadeRepository(DefaultContext _context)
        {
            context = _context;
        }

        public Produtividade GetById(int id)
        {
            return context.Produtividades
                .AsNoTracking()
                .Where(x => x.ProdutividadeId == id)
                .Include(x => x.Auditor)
                .Include(x => x.Produto)
                .Include(x => x.Orgao)
                .FirstOrDefault();
        }

        public List<Produtividade> GetAll()
        {
            return context.Produtividades
                .AsNoTracking()
                .Include(x => x.Auditor)
                .Include(x => x.Produto)
                .Include(x => x.Orgao)
                .ToList();
        }

        public void Save(Produtividade produtividade)
        {
            CalcularPontuacao(produtividade);

            if (produtividade.ProdutividadeId == 0)
            {
                context.Produtividades.Add(produtividade);
            }
            else
            {
                context.Attach(produtividade).State = EntityState.Modified;
                context.Update(produtividade);
            }

            context.SaveChanges();
        }

        public void Delete(int id)
        {
            var produtividade = context.Produtividades.Find(id);

            if (produtividade != null)
            {
                context.Produtividades.Remove(produtividade);
                context.SaveChanges();
            }
        }

        public ProdutividadeViewModel Converter(Produtividade produtividade)
        {
            return new ProdutividadeViewModel()
            {
                ProdutividadeId = produtividade.ProdutividadeId,
                AuditorId = produtividade.AuditorId,
                Auditor = new AuditorViewModel()
                {
                    Nome = produtividade.Auditor.Nome
                },
                ProdutoId = produtividade.ProdutoId,
                Produto = new ProdutoViewModel()
                {
                    Nome = produtividade.Produto.Nome
                },
                OrgaoId = produtividade.OrgaoId,
                Orgao = new OrgaoViewModel()
                {
                    Sigla = produtividade.Orgao.Sigla
                },
                UnidadeExecucao = produtividade.UnidadeExecucao,
                Complexidade = produtividade.Complexidade,
                HorasDipendidas = produtividade.HorasDipendidas,
                UnidadeOrcamentaria = produtividade.UnidadeOrcamentaria,
                Assunto = produtividade.Assunto,
                NumeroOrdem = produtividade.NumeroOrdem,
                AnoOrdem = produtividade.AnoOrdem,
                Prazo = produtividade.Prazo,
                Situacao = produtividade.Situacao,
                DataInicio = produtividade.DataInicio,
                DataTermino = produtividade.DataTermino,
                DataConclusao = produtividade.DataConclusao,
                NumeroProduto = produtividade.NumeroProduto,
                AnoProduto = produtividade.AnoProduto,
                ExpedienteOriginario = produtividade.ExpedienteOriginario,
                NumeroDemanda = produtividade.NumeroDemanda,
                AnoDemanda = produtividade.AnoDemanda,
                ObservacoesDemanda = produtividade.ObservacoesDemanda,
                ObservacoesGerais = produtividade.ObservacoesGerais,
                Pontuacao = produtividade.Pontuacao
            };
        }

        public Produtividade Converter(ProdutividadeViewModel produtividade)
        {
            var auditor = context.Auditores.Find(produtividade.AuditorId);
            var produto = context.Produtos.Find(produtividade.ProdutoId);
            var orgao = context.Orgaos.Find(produtividade.OrgaoId);

            return new Produtividade()
            {
                ProdutividadeId = produtividade.ProdutividadeId,
                AuditorId = produtividade.AuditorId,
                Auditor = auditor,
                ProdutoId = produtividade.ProdutoId,
                Produto = produto,
                OrgaoId = produtividade.OrgaoId,
                Orgao = orgao,
                UnidadeExecucao = produtividade.UnidadeExecucao,
                Complexidade = produtividade.Complexidade,
                HorasDipendidas = produtividade.HorasDipendidas,
                UnidadeOrcamentaria = produtividade.UnidadeOrcamentaria,
                Assunto = produtividade.Assunto,
                NumeroOrdem = produtividade.NumeroOrdem,
                AnoOrdem = produtividade.AnoOrdem,
                Prazo = produtividade.Prazo,
                Situacao = produtividade.Situacao,
                DataInicio = produtividade.DataInicio,
                DataTermino = produtividade.DataTermino,
                DataConclusao = produtividade.DataConclusao,
                NumeroProduto = produtividade.NumeroProduto,
                AnoProduto = produtividade.AnoProduto,
                ExpedienteOriginario = produtividade.ExpedienteOriginario,
                NumeroDemanda = produtividade.NumeroDemanda,
                AnoDemanda = produtividade.AnoDemanda,
                ObservacoesDemanda = produtividade.ObservacoesDemanda,
                ObservacoesGerais = produtividade.ObservacoesGerais,
                Pontuacao = produtividade.Pontuacao
            };
        }

        public List<ProdutividadeViewModel> Converter(List<Produtividade> produtividades)
        {
            var produtividadesView = new List<ProdutividadeViewModel>();

            produtividades
                .ForEach(x => produtividadesView.Add(new ProdutividadeViewModel()
                {
                    ProdutividadeId = x.ProdutividadeId,
                    AuditorId = x.AuditorId,
                    Auditor = new AuditorViewModel()
                    {
                        Nome = x.Auditor.Nome
                    },
                    ProdutoId = x.ProdutoId,
                    Produto = new ProdutoViewModel()
                    {
                        Nome = x.Produto.Nome
                    },
                    Orgao = new OrgaoViewModel()
                    {
                        Sigla = x.Orgao.Sigla
                    },
                    UnidadeExecucao = x.UnidadeExecucao,
                    Situacao = x.Situacao,
                    Pontuacao = x.Pontuacao
                }));

            return produtividadesView;
        }

        public void CalcularPontuacao(Produtividade produtividade)
        {
            if(produtividade.Produto.TipoPontuacao == "Fixa")
            {
                var complexidade = context.Complexidades
                    .Where(x => x.Classificacao == produtividade.Complexidade)
                    .FirstOrDefault();
                double pontuacao = produtividade.Produto.PontoBase * complexidade.Peso;
                produtividade.Pontuacao = pontuacao;
            }
            else if(produtividade.Produto.TipoPontuacao == "Variável")
            {
                double pontuacao = produtividade.Produto.PontoBase * (int) produtividade.HorasDipendidas;
                produtividade.Pontuacao = pontuacao;
            }
        }

        public List<Produtividade> GetProdutividadesByAuditor(int auditorId, string situacao)
        {
            List<Produtividade> produtividades = new List<Produtividade>();
            if (situacao != null)
                produtividades = context.Produtividades
                .Where(x => x.AuditorId == auditorId)
                .Where(x => x.Situacao == situacao)
                .ToList();
            else
                produtividades = context.Produtividades
                .Where(x => x.AuditorId == auditorId)
                .ToList();
            return produtividades;
        }

        public List<Produtividade> GetProdutividadesByProduto(int produtoId, string situacao)
        {
            List<Produtividade> produtividades = new List<Produtividade>();
            if (situacao != null)
                produtividades = context.Produtividades
                .Where(x => x.AuditorId == produtoId)
                .Where(x => x.Situacao == situacao)
                .ToList();
            else
                produtividades = context.Produtividades
                .Where(x => x.AuditorId == produtoId)
                .ToList();
            return produtividades;
        }

        public double CalcularFAP(double total)
        {
            if (((total / 250) * 10) > 10)
                return 10;
            else
                return (total / 250) * 10;
        }

        public bool ProdutividadeExist(int id)
        {
            return context.Produtividades
                .Where(x => x.ProdutividadeId == id)
                .Any();
        }
    }
}
