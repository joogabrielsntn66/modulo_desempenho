﻿using desempenho_cge.Context;
using desempenho_cge.Models;
using desempenho_cge.Models.Produtividade;
using desempenho_cge.ViewModels.Produtividade;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;

namespace desempenho_cge.Repositories
{
    public class ProdutoRepository
    {
        private readonly DefaultContext context;

        public ProdutoRepository(DefaultContext _context)
        {
            context = _context;
        }

        public Produto GetById(int id)
        {
            if(id > 0)
            {
                return context.Produtos
                    .AsNoTracking()
                    .Where(x => x.ProdutoId == id)
                    .Include(x => x.ProdutoComplexidades)
                        .ThenInclude(x => x.Complexidade)
                    .Single();
            }

            return null;
        }

        public List<ProdutoComplexidade> GetComplexidadesFromProduto()
        {
            return context.ProdutoComplexidades.Include(x => x.Complexidade).ToList();
        }

        public List<Produto> GetAll()
        {
            return context.Produtos
                .AsNoTracking()
                .OrderBy(x => x.Nome)
                .ToList();
        }

        public void Save(Produto produto, List<int> complexidades)
        {
            if(produto.ProdutoId == 0)
            {
                foreach(var complexidadeId in complexidades)
                {
                    var complexidade = context.Complexidades.Find(complexidadeId);
                    produto.ProdutoComplexidades.Add(new ProdutoComplexidade() { 
                        Complexidade = complexidade,
                        Produto = produto
                    });
                }
                context.Produtos.Add(produto);
            }
            else 
            {
                foreach(var item in produto.ProdutoComplexidades)
                {
                    context.Entry(item).State = EntityState.Deleted;
                }

                foreach (var complexidadeId in complexidades)
                {
                    var complexidade = context.Complexidades.Find(complexidadeId);

                    var item = new ProdutoComplexidade()
                    {
                        Complexidade = complexidade,
                        Produto = produto
                    };

                    produto.ProdutoComplexidades.Add(item);
                }
                
                context.Produtos.Attach(produto).State = EntityState.Modified;
                context.Produtos.Update(produto);
            }

            context.SaveChanges();
        }

        public void Delete(int id)
        {
            var produto = context.Produtos.Find(id);

            if(produto != null)
            {
                context.Produtos.Remove(produto);
                context.SaveChanges();
            }
        }

        public bool ProdutoExist(int id)
        {
            return context.Produtos
                .Where(x => x.ProdutoId == id)
                .Any();
        }

        public List<Produto> GetProdutos(List<int> produtosView)
        {
            List<Produto> produtos = new List<Produto>();
            if (produtosView.Count > 0)
            {
                foreach (var produtoView in produtosView)
                {
                    var produto = GetById(produtoView);
                    produtos.Add(produto);
                }
            }
            else
                produtos = GetAll();
            return produtos;
        }
    }
}
