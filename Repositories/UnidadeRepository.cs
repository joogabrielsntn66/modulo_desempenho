﻿using desempenho_cge.Context;
using desempenho_cge.Models;
using desempenho_cge.Util;
using desempenho_cge.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace desempenho_cge.Repositories
{
    public class UnidadeRepository
    {
        private readonly DefaultContext context;

        public UnidadeRepository(DefaultContext _context)
        {
            context = _context;
        }

        public Unidade GetById(int id)
        {
            return context.Unidades.
                AsNoTracking()
                .Where(x => x.UnidadeId == id).FirstOrDefault();
        }

        public List<Unidade> GetAll()
        {
            return context.Unidades
                .AsNoTracking()
                .OrderBy(x => x.UnidadeId)
                .ToList();
        }

        public void Save(Unidade unidade)
        {
            if(unidade.UnidadeId == 0)
            {
                context.Unidades.Add(unidade);
            }
            else
            {
                context.Attach(unidade).State = EntityState.Modified;
                context.Update(unidade);
            }

            context.SaveChanges();
        }

        public void Delete(int id)
        {
            var unidade = context.Unidades.Find(id);

            if(unidade != null)
            {
                context.Unidades.Remove(unidade);
                context.SaveChanges();
            }
        }

        public bool UnidadeExist(int id)
        {
            return context.Unidades
                .Where(x => x.UnidadeId == id)
                .Any();
        }

        public UnidadeViewModel Converter(Unidade unidade)
        {
            var unidadeView = new UnidadeViewModel();
            PropertyCopier<Unidade, UnidadeViewModel>.Copy(unidade, unidadeView);
            return unidadeView;
        }

        public Unidade Converter(UnidadeViewModel unidadeView)
        {
            var unidade = new Unidade();
            PropertyCopier<UnidadeViewModel, Unidade>.Copy(unidadeView, unidade);
            return unidade;
        }
    }
}
