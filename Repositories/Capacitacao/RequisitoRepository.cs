﻿using desempenho_cge.Context;
using desempenho_cge.Models;
using desempenho_cge.Util;
using desempenho_cge.ViewModels;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace desempenho_cge.Repositories
{
    public class RequisitoRepository
    {
        private readonly DefaultContext context;

        public RequisitoRepository(DefaultContext _context)
        {
            context = _context;
        }

        public Requisito GetById(int id)
        {
            return context.Requisitos
                .AsNoTracking()
                .Where(x => x.RequisitoId == id)
                .Include(x => x.Modalidade)
                .FirstOrDefault();
        }

        public List<Requisito> GetAll()
        {
            return context.Requisitos
                .AsNoTracking()
                .OrderBy(x => x.RequisitoId)
                .Include(x => x.Modalidade)
                .ToList();
        }

        public void Save(Requisito requisito) 
        {
            if(requisito.RequisitoId == 0)
            {
                context.Requisitos.Add(requisito);
            }
            else 
            {
                context.Attach(requisito).State = EntityState.Modified;
                context.Update(requisito);
            }

            context.SaveChanges();
        }

        public void Delete(int id) 
        {
            var requisito = context.Requisitos.Find(id);

            if(requisito != null)
            {
                context.Requisitos.Remove(requisito);
                context.SaveChanges();
            }
        }

        public bool RequisitoExist(int id)
        {
            return context.Requisitos
                .Where(x => x.RequisitoId == id)
                .Any();
        }

        public RequisitoViewModel Converter(Requisito requisito)
        {
            var requisitoView = new RequisitoViewModel();

            PropertyCopier<Requisito, RequisitoViewModel>
                .Copy(requisito, requisitoView);

            if (requisito.Modalidade != null)
                requisitoView.Modalidade = new ModalidadeViewModel
                { Nome = requisito.Modalidade.Nome };

            return requisitoView;
        }

        public Requisito Converter(RequisitoViewModel requisitoView)
        {
            var requisito = new Requisito();

            PropertyCopier<RequisitoViewModel, Requisito>
                .Copy(requisitoView, requisito);

            if (requisitoView.Modalidade != null)
                requisito.Modalidade = context.Modalidades
                    .Find(requisitoView.Modalidade.ModalidadeId);

            return requisito;
        }
    }
}
