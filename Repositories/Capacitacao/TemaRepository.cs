﻿using desempenho_cge.Context;
using desempenho_cge.Models;
using desempenho_cge.Util;
using desempenho_cge.ViewModels;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace desempenho_cge.Repositories
{
    public class TemaRepository
    {
        private readonly DefaultContext context;

        public TemaRepository(DefaultContext _context)
        {
            context = _context;
        }

        public Tema GetById(int id)
        {
            return context.Temas
                .AsNoTracking()
                .Where(x => x.TemaId == id)
                .Include(x => x.Eixo)
                .FirstOrDefault();
        }

        public List<Tema> GetAll()
        {
            return context.Temas
                .AsNoTracking()
                .OrderBy(x => x.TemaId)
                .Include(x => x.Eixo)
                .ToList();
        }

        public void Save(Tema tema)
        {
            if(tema.TemaId == 0)
            {
                context.Temas.Add(tema);
            }
            else
            {
                context.Attach(tema).State = EntityState.Modified;
                context.Update(tema);
            }

            context.SaveChanges();
        }

        public void Delete(int id)
        {
            var tema = context.Temas.Find(id);

            if(tema != null)
            {
                context.Temas.Remove(tema);
                context.SaveChanges();
            }
        }

        public bool TemaExist(int id)
        {
            return context.Temas
                .Where(x => x.TemaId == id)
                .Any();
        }

        public TemaViewModel Converter(Tema tema)
        {
            var temaView = new TemaViewModel();
            PropertyCopier<Tema, TemaViewModel>.Copy(tema, temaView);
            if(tema.Eixo != null)
                temaView.Eixo = new EixoViewModel { Nome = tema.Eixo.Nome };
            return temaView;
        }

        public Tema Converter(TemaViewModel temaView)
        {
            var tema = new Tema();
            PropertyCopier<TemaViewModel, Tema>.Copy(temaView, tema);
            if (temaView.Eixo != null)
                tema.Eixo = new Eixo { Nome = temaView.Eixo.Nome };
            return tema;
        }
    }
}
