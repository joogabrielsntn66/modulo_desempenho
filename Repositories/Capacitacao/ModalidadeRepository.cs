﻿using desempenho_cge.Context;
using desempenho_cge.Models;
using desempenho_cge.Util;
using desempenho_cge.ViewModels;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace desempenho_cge.Repositories
{
    public class ModalidadeRepository
    {
        private readonly DefaultContext context;

        public ModalidadeRepository(DefaultContext _context)
        {
            context = _context;
        }

        public Modalidade GetById(int id)
        {
            return context.Modalidades
                .AsNoTracking()
                .Where(x => x.ModalidadeId == id)
                .Include(x => x.Requisitos)
                .FirstOrDefault();
        }

        public List<Modalidade> GetAll()
        {
            return context.Modalidades
                .AsNoTracking()
                .OrderBy(x => x.ModalidadeId)
                .Include(x => x.Requisitos)
                .ToList();
        }

        public void Save(Modalidade modalidade)
        {
            if(modalidade.ModalidadeId == 0)
            {
                context.Modalidades.Add(modalidade);
            }
            else
            {
                context.Attach(modalidade).State = EntityState.Modified;
                context.Update(modalidade);
            }

            context.SaveChanges();
        }

        public void Delete(int id)
        {
            var obj = context.Modalidades.Find(id);

            if(obj != null)
            {
                context.Modalidades.Remove(obj);
                context.SaveChanges();
            }
        }

        public bool ModalidadeExist(int id)
        {
            return context.Modalidades
                .Where(x => x.ModalidadeId == id)
                .Any();
        }

        public ModalidadeViewModel Converter(Modalidade modalidade)
        {
            var modalidadeView = new ModalidadeViewModel();
            PropertyCopier<Modalidade, ModalidadeViewModel>
                .Copy(modalidade, modalidadeView);
            return modalidadeView;
        }

        public Modalidade Converter(ModalidadeViewModel modalidadeView)
        {
            var modalidade = new Modalidade();
            PropertyCopier<ModalidadeViewModel, Modalidade>
                .Copy(modalidadeView, modalidade);
            return modalidade;
        }
    }
}
