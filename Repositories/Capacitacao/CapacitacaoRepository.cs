﻿using desempenho_cge.Context;
using desempenho_cge.Models;
using desempenho_cge.Util;
using desempenho_cge.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace desempenho_cge.Repositories
{
    public class CapacitacaoRepository
    {
        private readonly DefaultContext context;

        public CapacitacaoRepository(DefaultContext _context)
        {
            context = _context;
        }

        public Capacitacao GetById(int id)
        {
            return context.Capacitacoes
                .AsNoTracking()
                .Where(x => x.CapacitacaoId == id)
                .Include(x => x.Auditor)
                .Include(x => x.Modalidade)
                .Include(x => x.Eixo)
                .FirstOrDefault();
        }

        public List<Capacitacao> GetAll()
        {
            return context.Capacitacoes
                .AsNoTracking()
                .Include(x => x.Auditor)
                .Include(x => x.Modalidade)
                .OrderBy(x => x.CapacitacaoId)
                .ToList();
        }

        public void Save(Capacitacao capacitacao)
        {
            CalcularPontuacao(capacitacao);

            if (capacitacao.CapacitacaoId == 0)
            {
                context.Capacitacoes.Add(capacitacao);
            }
            else
            {
                context.Attach(capacitacao).State = EntityState.Modified;
                context.Update(capacitacao);
            }

            context.SaveChanges();
        }

        public void Delete(int id)
        {
            var capacitacao = context.Capacitacoes.Find(id);

            if(capacitacao != null)
            {
                context.Capacitacoes.Remove(capacitacao);
                context.SaveChanges();
            }
        }

        public bool Exist(int id)
        {
            return context.Capacitacoes
                .Where(x => x.CapacitacaoId == id)
                .Any();
        }

        public void CalcularPontuacao(Capacitacao capacitacao)
        {
            capacitacao.Pontuacao = 0.0;
            var modalidade = context.Modalidades.Find(capacitacao.ModalidadeId);
            var requisito = context.Requisitos
                    .Where(x => x.Nome == capacitacao.Requisito)
                    .FirstOrDefault();
            var tema = context.Temas
                .Where(x => x.Nome == capacitacao.Tema)
                .FirstOrDefault();

            if (modalidade.TipoModalidade == "Pontuação Fixa")
            {
                capacitacao.Pontuacao = (double) modalidade.Pontos;
            }
            else if (modalidade.TipoModalidade == "Pontuação p/ Hora")
            {
                capacitacao.Pontuacao = requisito.Pontos * capacitacao.CargaHoraria;
            }
            else if (modalidade.TipoModalidade == "Pontuação p/ Relevância")
            {
                if (tema.Relevancia == "Prioritário")
                {
                    capacitacao.Pontuacao = (double)modalidade.Pontos * capacitacao.CargaHoraria;
                }
                else if(tema.Relevancia == "Intermediário")
                {
                    var pontoTemporario = (double) modalidade.Pontos * 0.5;
                    capacitacao.Pontuacao = pontoTemporario * capacitacao.CargaHoraria;
                }
            }
            else if(modalidade.TipoModalidade == "Pontuação p/ Item")
                capacitacao.Pontuacao = (double) requisito.Pontos;
        }

        public List<Capacitacao> GetCapacitacoes(int auditorId, string situacao)
        {
            List<Capacitacao> capacitacoes = new List<Capacitacao>();
            if (situacao != null)
                capacitacoes = context.Capacitacoes
                    .Where(x => x.AuditorId == auditorId)
                    .Where(x => x.Homologado == situacao)
                    .ToList();
            else
                capacitacoes = context.Capacitacoes
                    .Where(x => x.AuditorId == auditorId)
                    .ToList();
            return capacitacoes;
        }

        public double CalcularTotalAjustado(double total)
        {
            if (total > (200 * 0.5))
                return (200 * 0.5);
            else
                return total;
        }

        public double CalcularFAC(double total)
        {
            if (((total / 200) * 10) > 10)
                return 10;
            else
                return ((total / 200) * 10);
        }

        public CapacitacaoViewModel Converter(Capacitacao capacitacao)
        {
            var capacitacaoView = new CapacitacaoViewModel();

            PropertyCopier<Capacitacao, CapacitacaoViewModel>
                .Copy(capacitacao, capacitacaoView);

            if (capacitacao.Auditor != null)
                capacitacaoView.Auditor = new AuditorViewModel 
                { 
                    Nome = capacitacao.Auditor.Nome,
                    Matricula = capacitacao.Auditor.Matricula
                };

            if (capacitacao.Eixo != null)
                capacitacaoView.Eixo = new EixoViewModel 
                { Nome = capacitacao.Eixo.Nome };

            if (capacitacao.Modalidade != null)
                capacitacaoView.Modalidade = new ModalidadeViewModel 
                { Nome = capacitacao.Modalidade.Nome };

            return capacitacaoView;
        }

        public Capacitacao Converter(CapacitacaoViewModel capacitacaoView)
        {
            var capacitacao = new Capacitacao();

            PropertyCopier<CapacitacaoViewModel, Capacitacao>
                .Copy(capacitacaoView, capacitacao);

            return capacitacao;
        }
    }
}
