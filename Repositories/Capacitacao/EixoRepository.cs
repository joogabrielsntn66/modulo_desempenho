﻿using desempenho_cge.Context;
using desempenho_cge.Models;
using desempenho_cge.Util;
using desempenho_cge.ViewModels;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace desempenho_cge.Repositories
{
    public class EixoRepository
    {
        private readonly DefaultContext context;

        public EixoRepository(DefaultContext _context)
        {
            context = _context;
        }

        public Eixo GetById(int id)
        {
            return context.Eixos
                .AsNoTracking()
                .Where(x => x.EixoId == id)
                .Include(x => x.Temas)
                .FirstOrDefault();
        }

        public List<Eixo> GetAll()
        {
            return context.Eixos
                .AsNoTracking()
                .OrderBy(x => x.EixoId)
                .Include(x => x.Temas)
                .ToList();
        }

        public void Save(Eixo eixo)
        {
            if(eixo.EixoId == 0)
            {
                context.Eixos.Add(eixo);
            }
            else
            {
                context.Attach(eixo).State = EntityState.Modified;
                context.Update(eixo);
            }

            context.SaveChanges();
        }

        public void Delete(int id)
        {
            var eixo = context.Eixos.Find(id);

            if (eixo != null)
            {
                context.Eixos.Remove(eixo);
                context.SaveChanges();
            }
        }

        public bool EixoExist(int id)
        {
            return context.Eixos
                .Where(x => x.EixoId == id)
                .Any();
        }

        public EixoViewModel Converter(Eixo eixo)
        {
            var eixoViewModel = new EixoViewModel();
            PropertyCopier<Eixo, EixoViewModel>.Copy(eixo, eixoViewModel);
            return eixoViewModel;
        }

        public Eixo Converter(EixoViewModel eixoView)
        {
            var eixo = new Eixo();
            PropertyCopier<EixoViewModel, Eixo>.Copy(eixoView, eixo);
            return eixo;
        }
    }
}
