﻿using desempenho_cge.Context;
using desempenho_cge.Models;
using desempenho_cge.Util;
using desempenho_cge.ViewModels;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace desempenho_cge.Repositories
{
    public class AvaliacaoRepository
    {
        private readonly DefaultContext context;

        public AvaliacaoRepository(DefaultContext _context)
        {
            context = _context;
        }

        public Avaliacao GetById(int id)
            => context.Avaliacoes
            .AsNoTracking()
            .Where(x => x.AvaliacaoId == id)
            .Include(x => x.AppUser)
            .Include(x => x.Auditor)
            .Include(x => x.Produto)
            .Include(x => x.Questionario)
            .FirstOrDefault();

        public List<Avaliacao> GetAll()
            => context.Avaliacoes
            .AsNoTracking()
            .Include(x => x.AppUser)
            .Include(x => x.Auditor)
            .Include(x => x.Produto)
            .ToList();

        public void Save(Avaliacao avaliacao)
        {
            if(avaliacao.AvaliacaoId == 0)
            {
                context.Avaliacoes.Add(avaliacao);
            }
            else
            {
                context.Attach(avaliacao).State = EntityState.Modified;
                context.Update(avaliacao);
            }

            context.SaveChanges();
        }

        public void Delete(int id)
        {
            var avaliacao = context.Avaliacoes.Find(id);

            if(avaliacao != null)
            {
                context.Avaliacoes.Remove(avaliacao);
                context.SaveChanges();
            }
        }

        public bool Exist(int id)
            => context.Avaliacoes
            .Where(x => x.AvaliacaoId == id)
            .Any();

        public Avaliacao Converter(AvaliacaoViewModel avaliacaoView)
        {
            var avaliacao = new Avaliacao();
            PropertyCopier<AvaliacaoViewModel, Avaliacao>
                .Copy(avaliacaoView, avaliacao);
            return avaliacao;
        }

        public AvaliacaoViewModel Converter(Avaliacao avaliacao)
        {
            var avaliacaoView = new AvaliacaoViewModel();
            PropertyCopier<Avaliacao, AvaliacaoViewModel>
                .Copy(avaliacao, avaliacaoView);
            return avaliacaoView;
        }

        public void CreateAnswers(Avaliacao avaliacao)
        {
            var respostas = new List<Resposta>();

            foreach (var grupo in avaliacao.Questionario.Grupos)
            {
                respostas.Add(new Resposta
                {
                    AvaliacaoId = avaliacao.AvaliacaoId,
                    QuestaoId = grupo.QuestaoId,
                    QuestaoConteudo = grupo.Conteudo,
                });

                foreach (var questao in grupo.SubQuestoes)
                {
                    respostas.Add(new Resposta
                    {
                        AvaliacaoId = avaliacao.AvaliacaoId,
                        QuestaoId = questao.QuestaoId,
                        QuestaoConteudo = questao.Conteudo,
                    });

                    foreach (var subquestao in questao.SubQuestoes)
                    {
                        respostas.Add(new Resposta
                        {
                            AvaliacaoId = avaliacao.AvaliacaoId,
                            QuestaoId = subquestao.QuestaoId,
                            QuestaoConteudo = subquestao.Conteudo,
                        });
                    }
                }
            }

            respostas.OrderBy(x => x.QuestaoId);
            foreach (var resposta in respostas)
                context.Respostas.Add(resposta);
            context.SaveChanges();
        }
    }
}
