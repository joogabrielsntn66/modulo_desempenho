﻿using desempenho_cge.Context;
using desempenho_cge.Models;
using Microsoft.AspNetCore.Mvc.Razor.Compilation;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace desempenho_cge.Repositories
{
    public class RespostaRepository
    {
        private readonly DefaultContext context;

        public RespostaRepository(DefaultContext _context)
        {
            context = _context;
        }

        public Resposta GetById(int id)
            => context.Respostas
            .AsNoTracking()
            .Where(x => x.RespostaId == id)
            .Include(x => x.Questao)
                .ThenInclude(x => x.SubQuestoes)
            .FirstOrDefault();

        public List<Resposta> GetAll(int AvaliacaoId)
            => context.Respostas
            .AsNoTracking()
            .Where(x => x.AvaliacaoId == AvaliacaoId)
            .Include(x => x.Questao)
                .ThenInclude(x => x.SubQuestoes)
            .ToList();

        public void Save(Resposta resposta)
        {
            if(resposta.RespostaId == 0)
            {
                context.Respostas.Add(resposta);
            }
            else
            {
                context.Attach(resposta).State = EntityState.Modified;
                context.Update(resposta);
            }

            context.SaveChanges();
        }

        public void Delete(int id)
        {
            var resposta = context.Respostas.Find(id);

            if(resposta != null)
            {
                context.Respostas.Remove(resposta);
                context.SaveChanges();
            }
        }

        public bool Exist(int id)
            => context.Respostas
            .Where(x => x.RespostaId == id)
            .Any();

        public void CalcularPontuacao(List<Resposta> respostas)
        {
            var questoes = respostas.Where(x => x.Questao.Nivel == 1).ToList();
            foreach(var questao in questoes)
            {
                if(questao.Questao.SubQuestoes.Count > 0)
                {
                    var subquestoes = respostas.Where(x => x.Questao.QuestaoPaiId == questao.QuestaoId).ToList();
                    double pontuacao = 0.0;
                    int quantidade = 0;
                    foreach(var subquestao in subquestoes)
                    {
                        pontuacao += subquestao.Valor;
                        quantidade++;
                    }
                    questao.Valor = pontuacao / quantidade;
                }
            }

            var grupos = respostas.Where(x => x.Questao.Nivel == 0).ToList();            
            foreach(var grupo in grupos)
            {
                var questoesGrupo = respostas.Where(x => x.Questao.QuestaoPaiId == grupo.QuestaoId).ToList();
                double pontuacao = 0.0;
                int quantidade = 0;
                foreach(var questao in questoesGrupo)
                {
                    pontuacao += questao.Valor;
                    quantidade++;
                }
                grupo.Valor = pontuacao / quantidade;
            }
        }
    }
}
