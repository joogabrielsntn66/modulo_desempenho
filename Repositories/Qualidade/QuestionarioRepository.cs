﻿using desempenho_cge.Context;
using desempenho_cge.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace desempenho_cge.Repositories
{
    public class QuestionarioRepository
    {
        private readonly DefaultContext context;

        public QuestionarioRepository(DefaultContext _context)
        {
            context = _context;
        }

        public Questionario GetById(int id)
        {
            return context.Questionarios
                .Where(x => x.QuestionarioId == id)
                .Include(x => x.Grupos)
                    .ThenInclude(x => x.SubQuestoes)
                            .ThenInclude(x => x.SubQuestoes)
                .FirstOrDefault();
        }

        public List<Questionario> GetAll()
        {
            return context.Questionarios
                .OrderBy(x => x.QuestionarioId)
                .Include(x => x.Grupos)
                    .ThenInclude(x => x.SubQuestoes)
                        .ThenInclude(x => x.SubQuestoes)
                .ToList();
        }

        public void Save(Questionario questionario)
        {
            if (questionario.QuestionarioId == 0)
            {
                context.Questionarios.Add(questionario);
            }
            else
            {
                context.Attach(questionario).State = EntityState.Modified;
                context.Update(questionario);
            }

            context.SaveChanges();
        }

        public void Delete(int id)
        {
            var questionario = context.Questionarios
                .Where(x => x.QuestionarioId == id)
                .Include(x => x.Grupos)
                    .ThenInclude(x => x.SubQuestoes)
                            .ThenInclude(x => x.SubQuestoes)
                .FirstOrDefault();

            if (questionario != null)
            {
                context.Questionarios.Remove(questionario);
                context.SaveChanges();
            }
        }

        public bool Exist(int id) => context.Questionarios.Where(x => x.QuestionarioId == id).Any();
    }
}
