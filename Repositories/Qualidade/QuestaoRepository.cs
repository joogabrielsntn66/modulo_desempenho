﻿using desempenho_cge.Context;
using desempenho_cge.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace desempenho_cge.Repositories
{
    public class QuestaoRepository
    {
        private readonly DefaultContext context;

        public QuestaoRepository(DefaultContext _context)
        {
            context = _context;
        }

        public Questao GetById(int id)
        {
            return context.Questoes
                .Where(x => x.QuestaoId == id)
                .Include(x => x.QuestaoPai)
                .Include(x => x.SubQuestoes)
                    .ThenInclude(x => x.SubQuestoes)
                .FirstOrDefault();
        }

        public List<Questao> GetAll()
        {
            return context.Questoes
               .Include(x => x.SubQuestoes)
               .ToList();
        }

        public void Save(Questao questao)
        {
            if (questao.QuestaoId == 0)
            {
                context.Questoes.Add(questao);
            }
            else
            {
                context.Attach(questao).State = EntityState.Modified;
                context.Update(questao);
            }

            context.SaveChanges();
        }

        public void Delete(int id)
        {
            var questao = context.Questoes.Find(id);

            if (questao != null)
            {
                context.Questoes.Remove(questao);
                context.SaveChanges();
            }
        }

        public bool Exist(int id) => context.Questoes.Where(x => x.QuestaoId == id).Any();
    }
}
