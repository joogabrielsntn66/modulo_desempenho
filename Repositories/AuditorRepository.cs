﻿using desempenho_cge.Context;
using desempenho_cge.Models;
using desempenho_cge.Util;
using desempenho_cge.ViewModels;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace desempenho_cge.Repositories
{
    public class AuditorRepository
    {
        private readonly DefaultContext context;

        public AuditorRepository(DefaultContext _context)
        {
            context = _context;
        }

        public Auditor GetById(int id)
        {
            return context.Auditores
                .AsNoTracking()
                .Where(x => x.AuditorId == id)
                .Include(x => x.Unidade)
                .FirstOrDefault();
        }

        public List<Auditor> GetAll()
        {
            return context.Auditores
                .AsNoTracking()
                .OrderBy(x => x.AuditorId)
                .Include(x => x.Unidade)
                .ToList();
        }

        public void Save(Auditor auditor)
        {
            if(auditor.AuditorId == 0)
            {
                context.Auditores.Add(auditor);
            }
            else
            {
                context.Attach(auditor).State = EntityState.Modified;
                context.Update(auditor);
            }

            context.SaveChanges();
        }

        public void Delete(int id)
        {
            var auditor = context.Auditores.Find(id);

            if(auditor != null)
            {
                context.Auditores.Remove(auditor);
                context.SaveChanges();
            }
        }

        public bool AuditorExist(int id)
        {
            return context.Auditores
                .Where(x => x.AuditorId == id)
                .Any();
        }

        public List<Auditor> GetAuditores(List<int> auditoresView)
        {
            List<Auditor> auditores = new List<Auditor>();
            if (auditoresView.Count > 0)
            {
                foreach (var auditorView in auditoresView)
                {
                    var auditor = context.Auditores.Find(auditorView);
                    auditores.Add(auditor);
                }
            }
            else
                auditores = context.Auditores.ToList();
            return auditores;
        }

        public AuditorViewModel Converter(Auditor auditor)
        {
            var auditorView = new AuditorViewModel();
            PropertyCopier<Auditor, AuditorViewModel>.Copy(auditor, auditorView);
            if (auditor.Unidade != null)
                auditorView.Unidade = new UnidadeViewModel 
                { Nome = auditor.Unidade.Nome, Sigla = auditor.Unidade.Sigla };
            return auditorView;
        }

        public Auditor Converter(AuditorViewModel auditorView)
        {
            var auditor = new Auditor();
            PropertyCopier<AuditorViewModel, Auditor>.Copy(auditorView, auditor);
            if (auditorView.Unidade != null)
                auditor.Unidade = new Unidade
                { Nome = auditorView.Unidade.Nome, Sigla = auditorView.Unidade.Sigla };
            return auditor;
        }
    }
}
