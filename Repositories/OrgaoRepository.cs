﻿using desempenho_cge.Context;
using desempenho_cge.Models;
using desempenho_cge.Util;
using desempenho_cge.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace desempenho_cge.Repositories
{
    public class OrgaoRepository
    {
        private readonly DefaultContext context;

        public OrgaoRepository(DefaultContext _context)
        {
            context = _context;
        }

        public Orgao GetById(int id)
        {
            return context.Orgaos.
                AsNoTracking()
                .Where(x => x.OrgaoId == id).FirstOrDefault();
        }

        public List<Orgao> GetAll()
        {
            return context.Orgaos
                .AsNoTracking()
                .OrderBy(x => x.Nome)
                .ToList();
        }

        public void Save(Orgao orgao)
        {
            if (orgao.OrgaoId == 0)
            {
                context.Orgaos.Add(orgao);
            }
            else
            {
                context.Attach(orgao).State = EntityState.Modified;
                context.Update(orgao);
            }

            context.SaveChanges();
        }

        public void Delete(int id)
        {
            var orgao = context.Orgaos.Find(id);

            if (orgao != null)
            {
                context.Orgaos.Remove(orgao);
                context.SaveChanges();
            }
        }

        public bool OrgaoExist(int id)
        {
            return context.Orgaos
                .Where(x => x.OrgaoId == id)
                .Any();
        }

        public OrgaoViewModel Converter(Orgao orgao)
        {
            var orgaoView = new OrgaoViewModel();
            PropertyCopier<Orgao, OrgaoViewModel>.Copy(orgao, orgaoView);
            return orgaoView;
        }

        public Orgao Converter(OrgaoViewModel orgaoView)
        {
            var orgao = new Orgao();
            PropertyCopier<OrgaoViewModel, Orgao>.Copy(orgaoView, orgao);
            return orgao;
        }
    }
}
