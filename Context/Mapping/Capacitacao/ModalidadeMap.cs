﻿using desempenho_cge.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace desempenho_cge.Context.Mapping
{
    public class ModalidadeMap : IEntityTypeConfiguration<Modalidade>
    {
        public void Configure(EntityTypeBuilder<Modalidade> builder)
        {
            builder.HasKey(x => x.ModalidadeId);

            builder.Property(x => x.Nome)
                .HasColumnType("varchar(200)")
                .IsRequired();

            builder.Property(x => x.TipoModalidade)
                .HasColumnType("varchar(150)")
                .IsRequired();
        }
    }
}
