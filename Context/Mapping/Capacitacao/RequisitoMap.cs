﻿using desempenho_cge.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace desempenho_cge.Context.Mapping
{
    public class RequisitoMap : IEntityTypeConfiguration<Requisito>
    {
        public void Configure(EntityTypeBuilder<Requisito> builder)
        {
            builder.HasKey(x => x.RequisitoId);

            builder.Property(x => x.Nome)
                .HasColumnType("varchar(200)")
                .IsRequired();

            builder.Property(x => x.Pontos)
                .IsRequired();
        }
    }
}
