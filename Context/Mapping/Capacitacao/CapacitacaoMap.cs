﻿using desempenho_cge.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace desempenho_cge.Context.Mapping
{
    public class CapacitacaoMap : IEntityTypeConfiguration<Capacitacao>
    {
        public void Configure(EntityTypeBuilder<Capacitacao> builder)
        {
            builder.HasKey(x => x.CapacitacaoId);

            builder.Property(x => x.NomeCurso)
                .HasColumnType("varchar(200)")
                .IsRequired();

            builder.Property(x => x.DataInicio)
                .HasColumnType("date")
                .IsRequired();

            builder.Property(x => x.DataFim)
                .HasColumnType("date")
                .IsRequired();

            builder.Property(x => x.GrauRelevancia)
                .HasColumnType("varchar(20)")
                .IsRequired(false);

            builder.Property(x => x.InstituicaoPromotora)
                .HasColumnType("varchar(255)")
                .IsRequired();

            builder.Property(x => x.FormaCapacitacao)
                .HasColumnType("varchar(20)")
                .IsRequired(false);

            builder.Property(x => x.Observacao)
                .HasColumnType("text")
                .IsRequired(false);

            builder.Property(x => x.Homologado)
                .HasColumnType("varchar(25)")
                .IsRequired();

            builder.Property(x => x.DocumentoPath)
                .HasColumnType("text")
                .IsRequired(false);
        }
    }
}
