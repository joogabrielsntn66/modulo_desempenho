﻿using desempenho_cge.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace desempenho_cge.Context.Mapping
{
    public class EixoMap : IEntityTypeConfiguration<Eixo>
    {
        public void Configure(EntityTypeBuilder<Eixo> builder)
        {
            builder.HasKey(x => x.EixoId);

            builder.Property(x => x.Nome)
                .HasColumnType("varchar(200)")
                .IsRequired();
        }
    }
}
