﻿using desempenho_cge.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace desempenho_cge.Context.Mapping
{
    public class RespostaMap : IEntityTypeConfiguration<Resposta>
    {
        public void Configure(EntityTypeBuilder<Resposta> builder)
        {
            builder.HasKey(x => x.RespostaId);

            builder.Property(x => x.QuestaoConteudo)
                .HasColumnType("text")
                .IsRequired(false);

            builder.Property(x => x.Observacao)
                .HasColumnType("text")
                .IsRequired(false);
        }
    }
}
