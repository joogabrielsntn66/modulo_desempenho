﻿using desempenho_cge.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace desempenho_cge.Context.Mapping
{
    public class AvaliacaoMap : IEntityTypeConfiguration<Avaliacao>
    {
        public void Configure(EntityTypeBuilder<Avaliacao> builder)
        {
            builder.HasKey(x => x.AvaliacaoId);

            builder.Property(x => x.NumeroProduto)
                .HasColumnType("varchar(10)")
                .IsRequired(false);

            builder.Property(x => x.AnoProduto)
                .HasColumnType("varchar(10)")
                .IsRequired(false);

            builder.Property(x => x.DataEntrega)
                .HasColumnType("date")
                .IsRequired();

            builder.Property(x => x.UnidadeExecucao)
                .HasColumnType("varchar(255)")
                .IsRequired(false);

            builder.Property(x => x.NumeroServico)
                .HasColumnType("varchar(10)")
                .IsRequired(false);

            builder.Property(x => x.AnoServico)
                .HasColumnType("varchar(10)")
                .IsRequired(false);
        }
    }
}
