﻿using desempenho_cge.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace desempenho_cge.Context.Mapping
{
    public class QuestaoMap : IEntityTypeConfiguration<Questao>
    {
        public void Configure(EntityTypeBuilder<Questao> builder)
        {
            builder.HasKey(q => q.QuestaoId);

            builder.Property(q => q.Conteudo)
                .HasColumnType("text")
                .IsRequired();

            builder.Property(q => q.Nivel)
                .IsRequired();
        }
    }
}
