﻿using desempenho_cge.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace desempenho_cge.Context.Mapping
{
    public class OrgaoMap : IEntityTypeConfiguration<Orgao>
    {
        public void Configure(EntityTypeBuilder<Orgao> builder)
        {
            builder.HasKey(x => x.OrgaoId);

            builder.Property(x => x.Nome)
                .HasColumnType("varchar(200)")
                .IsRequired();

            builder.Property(x => x.Sigla)
                .HasColumnType("varchar(20)")
                .IsRequired();
        }
    }
}
