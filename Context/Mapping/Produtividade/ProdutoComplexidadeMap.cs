﻿using desempenho_cge.Models.Produtividade;
using Microsoft.EntityFrameworkCore;

namespace desempenho_cge.Context.Mapping
{
    public class ProdutoComplexidadeMap
    {
        public static void Mapping(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProdutoComplexidade>()
                .HasKey(x => new { x.ProdutoId, x.ComplexidadeId });

            modelBuilder.Entity<ProdutoComplexidade>()
                .HasOne(x => x.Produto)
                .WithMany(x => x.ProdutoComplexidades)
                .HasForeignKey(x => x.ProdutoId);

            modelBuilder.Entity<ProdutoComplexidade>()
                .HasOne(x => x.Complexidade)
                .WithMany(x => x.ProdutoComplexidades)
                .HasForeignKey(x => x.ComplexidadeId);
        }
    }
}
