﻿using desempenho_cge.Models.Produtividade;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace desempenho_cge.Context.Mapping
{
    public class ComplexidadeMap : IEntityTypeConfiguration<Complexidade>
    {
        public void Configure(EntityTypeBuilder<Complexidade> builder)
        {
            builder.HasKey(x => x.ComplexidadeId);

            builder.Property(x => x.Classificacao)
                .HasColumnType("varchar(10)")
                .IsRequired();

            builder.Property(x => x.Peso)
                .IsRequired();
        }
    }
}
