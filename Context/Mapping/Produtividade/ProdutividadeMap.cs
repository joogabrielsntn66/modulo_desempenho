﻿using desempenho_cge.Models.Produtividade;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace desempenho_cge.Context.Mapping
{
    public class ProdutividadeMap : IEntityTypeConfiguration<Produtividade>
    {
        public void Configure(EntityTypeBuilder<Produtividade> builder)
        {
            builder.HasKey(x => x.ProdutividadeId);

            builder.Property(x => x.Assunto)
                .HasColumnType("text")
                .IsRequired();

            builder.Property(x => x.UnidadeExecucao)
                .HasColumnType("varchar(200)")
                .IsRequired();

            builder.Property(x => x.Complexidade)
                .HasColumnType("varchar(10)")
                .IsRequired();

            builder.Property(x => x.UnidadeOrcamentaria)
                .HasColumnType("varchar(200)").IsRequired(false);

            builder.Property(x => x.NumeroOrdem)
                .HasColumnType("varchar(10)").IsRequired(false);

            builder.Property(x => x.AnoOrdem)
                .HasColumnType("varchar(10)").IsRequired(false);

            builder.Property(x => x.Situacao)
                .HasColumnType("varchar(100)")
                .IsRequired();

            builder.Property(x => x.DataInicio)
                .HasColumnType("date")
                .IsRequired();

            builder.Property(x => x.DataTermino)
                .HasColumnType("date")
                .HasDefaultValue(DateTime.MinValue);

            builder.Property(x => x.DataConclusao)
                .HasColumnType("date")
                .HasDefaultValue(DateTime.MinValue);

            builder.Property(x => x.NumeroProduto)
                .HasColumnType("varchar(10)").IsRequired(false);

            builder.Property(x => x.AnoProduto)
                .HasColumnType("varchar(10)").IsRequired(false);

            builder.Property(x => x.NumeroDemanda)
                .HasColumnType("varchar(10)").IsRequired(false);

            builder.Property(x => x.AnoDemanda)
                .HasColumnType("varchar(10)").IsRequired(false);

            builder.Property(x => x.ObservacoesDemanda)
                .HasColumnType("text").IsRequired(false);

            builder.Property(x => x.Pontuacao)
                .IsRequired();
        }
    }
}
