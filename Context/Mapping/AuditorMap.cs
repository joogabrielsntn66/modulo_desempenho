﻿using desempenho_cge.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace desempenho_cge.Context.Mapping
{
    public class AuditorMap : IEntityTypeConfiguration<Auditor>
    {
        public void Configure(EntityTypeBuilder<Auditor> builder)
        {
            builder.HasKey(x => x.AuditorId);

            builder.Property(x => x.Matricula)
                .HasColumnType("varchar(20)")
                .IsRequired();

            builder.Property(x => x.Nome)
                .HasColumnType("varchar(255)")
                .IsRequired();
        }
    }
}
