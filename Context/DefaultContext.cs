﻿using desempenho_cge.Context.Mapping;
using desempenho_cge.Models;
using desempenho_cge.Models.Account;
using desempenho_cge.Models.Produtividade;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace desempenho_cge.Context
{
    public class DefaultContext : IdentityDbContext<AppUser>
    {
        public DefaultContext(DbContextOptions<DefaultContext> options) : base(options) { }

        public DbSet<Produto> Produtos { get; set; }
        public DbSet<Complexidade> Complexidades { get; set; }
        public DbSet<ProdutoComplexidade> ProdutoComplexidades { get; set; }
        public DbSet<Unidade> Unidades { get; set; }
        public DbSet<Auditor> Auditores { get; set; }
        public DbSet<Produtividade> Produtividades { get; set; }
        public DbSet<Orgao> Orgaos { get; set; }
        public DbSet<Eixo> Eixos { get; set; }
        public DbSet<Tema> Temas { get; set; }
        public DbSet<Modalidade> Modalidades { get; set; }
        public DbSet<Requisito> Requisitos { get; set; }
        public DbSet<Capacitacao> Capacitacoes { get; set; }
        public DbSet<Questionario> Questionarios { get; set; }
        public DbSet<Questao> Questoes { get; set; }
        public DbSet<Avaliacao> Avaliacoes { get; set; }
        public DbSet<Resposta> Respostas { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Mapeando as Entidades para o banco de dados
            // Produtividade
            ProdutoComplexidadeMap.Mapping(modelBuilder);
            modelBuilder.ApplyConfiguration(new ProdutividadeMap());
            modelBuilder.ApplyConfiguration(new ComplexidadeMap());
            modelBuilder.ApplyConfiguration(new ProdutoMap());

            // Sistema
            modelBuilder.ApplyConfiguration(new UnidadeMap());
            modelBuilder.ApplyConfiguration(new AuditorMap());
            modelBuilder.ApplyConfiguration(new OrgaoMap());

            // Capacitação
            modelBuilder.ApplyConfiguration(new EixoMap());
            modelBuilder.ApplyConfiguration(new TemaMap());
            modelBuilder.ApplyConfiguration(new ModalidadeMap());
            modelBuilder.ApplyConfiguration(new RequisitoMap());
            modelBuilder.ApplyConfiguration(new CapacitacaoMap());

            // Qualidade
            modelBuilder.ApplyConfiguration(new QuestionarioMap());
            modelBuilder.ApplyConfiguration(new QuestaoMap());
            modelBuilder.ApplyConfiguration(new AvaliacaoMap());
            modelBuilder.ApplyConfiguration(new RespostaMap());

            // Adicionando configurações específicas para Identity
            modelBuilder.Entity<AppUser>()
                .HasIndex(b => b.Cpf)
                .IsUnique();
        }
    }
}
