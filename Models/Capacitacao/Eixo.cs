﻿using System.Collections;
using System.Collections.Generic;

namespace desempenho_cge.Models
{
    public class Eixo
    {
        public int EixoId { get; set; }
        public string Nome { get; set; }
        public ICollection<Tema> Temas { get; set; }
    }
}
