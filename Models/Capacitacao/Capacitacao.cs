﻿using System;

namespace desempenho_cge.Models
{
    public class Capacitacao
    {
        public int CapacitacaoId { get; set; }
        public int? EixoId { get; set; }
        public virtual Eixo Eixo { get; set; }
        public string Tema { get; set; }
        public int ModalidadeId { get; set; }
        public virtual Modalidade Modalidade { get; set; }
        public string Requisito { get; set; }
        public int AuditorId { get; set; }
        public virtual Auditor Auditor { get; set; }
        public string NomeCurso { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }
        public int Dias { get; set; }
        public int CargaHoraria { get; set; }
        public string GrauRelevancia { get; set; }
        public string InstituicaoPromotora { get; set; }
        public string FormaCapacitacao { get; set; }
        public string Observacao { get; set; }
        public string Homologado { get; set; }
        public string DocumentoPath { get; set; }
        public double Pontuacao { get; set; }
    }
}
