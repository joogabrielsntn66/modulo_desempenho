﻿namespace desempenho_cge.Models
{
    public class Tema
    {
        public int TemaId { get; set; }
        public string Nome { get; set; }
        public string Relevancia { get; set; }
        public int EixoId { get; set; }
        public virtual Eixo Eixo { get; set; }
    }
}
