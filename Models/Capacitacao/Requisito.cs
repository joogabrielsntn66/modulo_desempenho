﻿namespace desempenho_cge.Models
{
    public class Requisito
    {
        public int RequisitoId { get; set; }
        public string Nome { get; set; }
        public double Pontos { get; set; }
        public int ModalidadeId { get; set; }
        public virtual Modalidade Modalidade { get; set; }
    }
}
