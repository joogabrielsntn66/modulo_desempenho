﻿using System.Collections.Generic;

namespace desempenho_cge.Models
{
    public class Modalidade
    {
        public int ModalidadeId { get; set; }
        public string Nome { get; set; }
        public string TipoModalidade { get; set; }
        public double? Pontos { get; set; }
        public ICollection<Requisito> Requisitos { get; set; }
    }
}
