﻿namespace desempenho_cge.Models.Produtividade
{
    public class ProdutoComplexidade
    {
        public int ProdutoId { get; set; }
        public virtual Produto Produto { get; set; }
        public int ComplexidadeId { get; set; }
        public virtual Complexidade Complexidade { get; set; }
    }
}
