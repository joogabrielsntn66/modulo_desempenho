﻿using System.Collections.Generic;

namespace desempenho_cge.Models.Produtividade
{
    public class Complexidade
    {
        public int ComplexidadeId { get; set; }
        public string Classificacao { get; set; }
        public double Peso { get; set; }
        public virtual ICollection<ProdutoComplexidade> ProdutoComplexidades { get; set; }
    }
}
