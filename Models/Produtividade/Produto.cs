﻿using System.Collections.Generic;

namespace desempenho_cge.Models.Produtividade
{
    public class Produto
    {
        public int ProdutoId { get; set; }
        public string Nome { get; set; }
        public int Esforco { get; set; }
        public double PontoBase { get; set; }
        public string TipoPontuacao { get; set; }
        public virtual ICollection<ProdutoComplexidade> ProdutoComplexidades { get; set; }

        public Produto()
        {
            ProdutoComplexidades = new HashSet<ProdutoComplexidade>();
        }
    }
}
