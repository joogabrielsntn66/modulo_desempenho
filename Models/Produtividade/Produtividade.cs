﻿using System;

namespace desempenho_cge.Models.Produtividade
{
    public class Produtividade
    {
        public int ProdutividadeId { get; set; }
        public int AuditorId { get; set; }
        public virtual Auditor Auditor { get; set; }
        public int ProdutoId { get; set; }
        public virtual Produto Produto { get; set; }
        public int OrgaoId { get; set; }
        public string UnidadeOrcamentaria { get; set; }
        public virtual Orgao Orgao { get; set; }
        public string UnidadeExecucao { get; set; }
        public string Complexidade { get; set; }
        public int? HorasDipendidas { get; set; }
        public string Assunto { get; set; }
        public string NumeroOrdem { get; set; }
        public string AnoOrdem { get; set; }
        public int Prazo { get; set; }
        public string Situacao { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime? DataTermino { get; set; }
        public DateTime? DataConclusao { get; set; }
        public string NumeroProduto { get; set; }
        public string AnoProduto { get; set; }
        public string ExpedienteOriginario { get; set; }
        public string NumeroDemanda { get; set; }
        public string AnoDemanda { get; set; }
        public string ObservacoesDemanda { get; set; }
        public string ObservacoesGerais { get; set; }
        public double Pontuacao { get; set; }
    }
}
