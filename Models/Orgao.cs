﻿namespace desempenho_cge.Models
{
    public class Orgao
    {
        public int OrgaoId { get; set; }
        public string Nome { get; set; }
        public string Sigla { get; set; }
    }
}
