﻿namespace desempenho_cge.Models
{
    public class Unidade
    {
        public int UnidadeId { get; set; }
        public string Nome { get; set; }
        public string Sigla { get; set; }
    }
}
