﻿using System.Collections.Generic;

namespace desempenho_cge.Models
{
    public class Questao
    {
        public int QuestaoId { get; set; }
        public string Conteudo { get; set; }
        public int Nivel { get; set; }

        public int? QuestaoPaiId { get; set; }
        public virtual Questao QuestaoPai { get; set; }
        public virtual ICollection<Questao> SubQuestoes { get; set; }

        public int? QuestionarioId { get; set; }
        public virtual Questionario Questionario { get; set; }
    }
}
