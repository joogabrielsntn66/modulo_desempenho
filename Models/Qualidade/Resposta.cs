﻿namespace desempenho_cge.Models
{
    public class Resposta
    {
        public int RespostaId { get; set; }
        public int QuestaoId { get; set; }
        public virtual Questao Questao { get; set; }
        public double Valor { get; set; }
        public string Observacao { get; set; }
        public string QuestaoConteudo { get; set; }
        public int AvaliacaoId { get; set; }
        public virtual Avaliacao Avaliacao { get; set; }
    }
}
