﻿using desempenho_cge.Models.Account;
using desempenho_cge.Models.Produtividade;
using System;
using System.Collections.Generic;

namespace desempenho_cge.Models
{
    public class Avaliacao
    {
        public int AvaliacaoId { get; set; }
        public int ProdutoId { get; set; }
        public virtual Produto Produto { get; set; }
        public string NumeroProduto { get; set; }
        public string AnoProduto { get; set; }
        public DateTime DataEntrega { get; set; }
        public string NumeroServico { get; set; }
        public string AnoServico { get; set; }
        public int AuditorId { get; set; }
        public virtual Auditor Auditor { get; set; }
        public string UnidadeExecucao { get; set; }
        public string AppUserId { get; set; }
        public virtual AppUser AppUser { get; set; }
        public int QuestionarioId { get; set; }
        public virtual Questionario Questionario { get; set; }
        public virtual ICollection<Resposta> Respostas { get; set; }
    }
}
