﻿using System.Collections.Generic;

namespace desempenho_cge.Models
{
    public class Questionario
    {
        public int QuestionarioId { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public ICollection<Questao> Grupos { get; set; }
    }
}
