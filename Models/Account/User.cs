﻿using System.ComponentModel.DataAnnotations;

namespace desempenho_cge.Models.Account
{
    public class User
    {
        [Required]
        [Display(Name = "C.P.F")]
        public string Cpf { get; set; }

        [Required]
        [Display(Name = "Nome de Usuário")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "E-Mail")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Senha")]
        public string Password { get; set; }
    }
}
