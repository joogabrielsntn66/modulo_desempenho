﻿using Microsoft.AspNetCore.Identity;

namespace desempenho_cge.Models.Account
{
    public class AppUser : IdentityUser
    {
        public string Cpf { get; set; }
    }
}
