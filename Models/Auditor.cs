﻿namespace desempenho_cge.Models
{
    public class Auditor
    {
        public int AuditorId { get; set; }
        public string Matricula { get; set; }
        public string Nome { get; set; }
        public int UnidadeId { get; set; }
        public virtual Unidade Unidade { get; set; }
    }
}
