﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace desempenho_cge.Migrations
{
    public partial class Produtividade_Modulo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Complexidades",
                columns: table => new
                {
                    ComplexidadeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Classificacao = table.Column<string>(type: "varchar(10)", nullable: false),
                    Peso = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Complexidades", x => x.ComplexidadeId);
                });

            migrationBuilder.CreateTable(
                name: "Orgaos",
                columns: table => new
                {
                    OrgaoId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(type: "varchar(200)", nullable: false),
                    Sigla = table.Column<string>(type: "varchar(20)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orgaos", x => x.OrgaoId);
                });

            migrationBuilder.CreateTable(
                name: "Produtos",
                columns: table => new
                {
                    ProdutoId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(type: "varchar(200)", nullable: false),
                    Esforco = table.Column<int>(nullable: false),
                    PontoBase = table.Column<double>(nullable: false),
                    TipoPontuacao = table.Column<string>(type: "varchar(20)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Produtos", x => x.ProdutoId);
                });

            migrationBuilder.CreateTable(
                name: "Unidades",
                columns: table => new
                {
                    UnidadeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(type: "varchar(200)", nullable: false),
                    Sigla = table.Column<string>(type: "varchar(20)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Unidades", x => x.UnidadeId);
                });

            migrationBuilder.CreateTable(
                name: "ProdutoComplexidades",
                columns: table => new
                {
                    ProdutoId = table.Column<int>(nullable: false),
                    ComplexidadeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProdutoComplexidades", x => new { x.ProdutoId, x.ComplexidadeId });
                    table.ForeignKey(
                        name: "FK_ProdutoComplexidades_Complexidades_ComplexidadeId",
                        column: x => x.ComplexidadeId,
                        principalTable: "Complexidades",
                        principalColumn: "ComplexidadeId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProdutoComplexidades_Produtos_ProdutoId",
                        column: x => x.ProdutoId,
                        principalTable: "Produtos",
                        principalColumn: "ProdutoId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Auditores",
                columns: table => new
                {
                    AuditorId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Matricula = table.Column<string>(type: "varchar(20)", nullable: false),
                    Nome = table.Column<string>(type: "varchar(255)", nullable: false),
                    UnidadeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Auditores", x => x.AuditorId);
                    table.ForeignKey(
                        name: "FK_Auditores_Unidades_UnidadeId",
                        column: x => x.UnidadeId,
                        principalTable: "Unidades",
                        principalColumn: "UnidadeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Produtividades",
                columns: table => new
                {
                    ProdutividadeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AuditorId = table.Column<int>(nullable: false),
                    ProdutoId = table.Column<int>(nullable: false),
                    OrgaoId = table.Column<int>(nullable: false),
                    UnidadeOrcamentaria = table.Column<string>(type: "varchar(200)", nullable: true),
                    UnidadeExecucao = table.Column<string>(type: "varchar(200)", nullable: false),
                    Complexidade = table.Column<string>(type: "varchar(10)", nullable: false),
                    HorasDipendidas = table.Column<int>(nullable: false),
                    Assunto = table.Column<string>(type: "text", nullable: false),
                    NumeroOrdem = table.Column<string>(type: "varchar(10)", nullable: true),
                    AnoOrdem = table.Column<string>(type: "varchar(10)", nullable: true),
                    Prazo = table.Column<int>(nullable: false),
                    Situacao = table.Column<string>(type: "varchar(100)", nullable: false),
                    DataInicio = table.Column<DateTime>(type: "date", nullable: false),
                    DataTermino = table.Column<DateTime>(type: "date", nullable: true, defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)),
                    DataConclusao = table.Column<DateTime>(type: "date", nullable: true, defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)),
                    NumeroProduto = table.Column<string>(type: "varchar(10)", nullable: true),
                    AnoProduto = table.Column<string>(type: "varchar(10)", nullable: true),
                    ExpedienteOriginario = table.Column<string>(nullable: true),
                    NumeroDemanda = table.Column<string>(type: "varchar(10)", nullable: true),
                    AnoDemanda = table.Column<string>(type: "varchar(10)", nullable: true),
                    ObservacoesDemanda = table.Column<string>(type: "text", nullable: true),
                    ObservacoesGerais = table.Column<string>(nullable: true),
                    Pontuacao = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Produtividades", x => x.ProdutividadeId);
                    table.ForeignKey(
                        name: "FK_Produtividades_Auditores_AuditorId",
                        column: x => x.AuditorId,
                        principalTable: "Auditores",
                        principalColumn: "AuditorId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Produtividades_Orgaos_OrgaoId",
                        column: x => x.OrgaoId,
                        principalTable: "Orgaos",
                        principalColumn: "OrgaoId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Produtividades_Produtos_ProdutoId",
                        column: x => x.ProdutoId,
                        principalTable: "Produtos",
                        principalColumn: "ProdutoId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Auditores_UnidadeId",
                table: "Auditores",
                column: "UnidadeId");

            migrationBuilder.CreateIndex(
                name: "IX_Produtividades_AuditorId",
                table: "Produtividades",
                column: "AuditorId");

            migrationBuilder.CreateIndex(
                name: "IX_Produtividades_OrgaoId",
                table: "Produtividades",
                column: "OrgaoId");

            migrationBuilder.CreateIndex(
                name: "IX_Produtividades_ProdutoId",
                table: "Produtividades",
                column: "ProdutoId");

            migrationBuilder.CreateIndex(
                name: "IX_ProdutoComplexidades_ComplexidadeId",
                table: "ProdutoComplexidades",
                column: "ComplexidadeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Produtividades");

            migrationBuilder.DropTable(
                name: "ProdutoComplexidades");

            migrationBuilder.DropTable(
                name: "Auditores");

            migrationBuilder.DropTable(
                name: "Orgaos");

            migrationBuilder.DropTable(
                name: "Complexidades");

            migrationBuilder.DropTable(
                name: "Produtos");

            migrationBuilder.DropTable(
                name: "Unidades");
        }
    }
}
