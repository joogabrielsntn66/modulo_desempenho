﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace desempenho_cge.Migrations
{
    public partial class Eixo_Tema : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Eixos",
                columns: table => new
                {
                    EixoId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(type: "varchar(200)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Eixos", x => x.EixoId);
                });

            migrationBuilder.CreateTable(
                name: "Temas",
                columns: table => new
                {
                    TemaId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(type: "varchar(200)", nullable: false),
                    Relevancia = table.Column<string>(type: "varchar(30)", nullable: false),
                    EixoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Temas", x => x.TemaId);
                    table.ForeignKey(
                        name: "FK_Temas_Eixos_EixoId",
                        column: x => x.EixoId,
                        principalTable: "Eixos",
                        principalColumn: "EixoId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Temas_EixoId",
                table: "Temas",
                column: "EixoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Temas");

            migrationBuilder.DropTable(
                name: "Eixos");
        }
    }
}
