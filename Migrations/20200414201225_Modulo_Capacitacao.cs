﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace desempenho_cge.Migrations
{
    public partial class Modulo_Capacitacao : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Capacitacoes",
                columns: table => new
                {
                    CapacitacaoId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EixoId = table.Column<int>(nullable: true),
                    ModalidadeId = table.Column<int>(nullable: false),
                    AuditorId = table.Column<int>(nullable: false),
                    NomeCurso = table.Column<string>(type: "varchar(200)", nullable: false),
                    DataInicio = table.Column<DateTime>(type: "date", nullable: false),
                    DataFim = table.Column<DateTime>(type: "date", nullable: false),
                    Dias = table.Column<int>(nullable: false),
                    GrauRelevancia = table.Column<string>(type: "varchar(20)", nullable: true),
                    InstituicaoPromotora = table.Column<string>(type: "varchar(255)", nullable: false),
                    FormaCapacitacao = table.Column<string>(type: "varchar(20)", nullable: true),
                    Observacao = table.Column<string>(type: "text", nullable: true),
                    Homologado = table.Column<bool>(nullable: false),
                    Pontuacao = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Capacitacoes", x => x.CapacitacaoId);
                    table.ForeignKey(
                        name: "FK_Capacitacoes_Auditores_AuditorId",
                        column: x => x.AuditorId,
                        principalTable: "Auditores",
                        principalColumn: "AuditorId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Capacitacoes_Eixos_EixoId",
                        column: x => x.EixoId,
                        principalTable: "Eixos",
                        principalColumn: "EixoId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Capacitacoes_Modalidades_ModalidadeId",
                        column: x => x.ModalidadeId,
                        principalTable: "Modalidades",
                        principalColumn: "ModalidadeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Capacitacoes_AuditorId",
                table: "Capacitacoes",
                column: "AuditorId");

            migrationBuilder.CreateIndex(
                name: "IX_Capacitacoes_EixoId",
                table: "Capacitacoes",
                column: "EixoId");

            migrationBuilder.CreateIndex(
                name: "IX_Capacitacoes_ModalidadeId",
                table: "Capacitacoes",
                column: "ModalidadeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Capacitacoes");
        }
    }
}
