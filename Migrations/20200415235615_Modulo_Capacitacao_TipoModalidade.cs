﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace desempenho_cge.Migrations
{
    public partial class Modulo_Capacitacao_TipoModalidade : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "Pontos",
                table: "Modalidades",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TipoModalidade",
                table: "Modalidades",
                type: "varchar(150)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "CargaHoraria",
                table: "Capacitacoes",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Pontos",
                table: "Modalidades");

            migrationBuilder.DropColumn(
                name: "TipoModalidade",
                table: "Modalidades");

            migrationBuilder.DropColumn(
                name: "CargaHoraria",
                table: "Capacitacoes");
        }
    }
}
