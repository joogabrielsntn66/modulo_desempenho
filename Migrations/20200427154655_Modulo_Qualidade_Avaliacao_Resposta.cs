﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace desempenho_cge.Migrations
{
    public partial class Modulo_Qualidade_Avaliacao_Resposta : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Avaliacoes",
                columns: table => new
                {
                    AvaliacaoId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProdutoId = table.Column<int>(nullable: false),
                    NumeroProduto = table.Column<string>(type: "varchar(10)", nullable: true),
                    AnoProduto = table.Column<string>(type: "varchar(10)", nullable: true),
                    DataEntrega = table.Column<DateTime>(type: "date", nullable: false),
                    NumeroServico = table.Column<string>(type: "varchar(10)", nullable: true),
                    AnoServico = table.Column<string>(type: "varchar(10)", nullable: true),
                    AuditorId = table.Column<int>(nullable: false),
                    UnidadeExecucao = table.Column<string>(type: "varchar(255)", nullable: true),
                    AppUserId = table.Column<string>(nullable: true),
                    QuestionarioId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Avaliacoes", x => x.AvaliacaoId);
                    table.ForeignKey(
                        name: "FK_Avaliacoes_AspNetUsers_AppUserId",
                        column: x => x.AppUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Avaliacoes_Auditores_AuditorId",
                        column: x => x.AuditorId,
                        principalTable: "Auditores",
                        principalColumn: "AuditorId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Avaliacoes_Produtos_ProdutoId",
                        column: x => x.ProdutoId,
                        principalTable: "Produtos",
                        principalColumn: "ProdutoId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Avaliacoes_Questionarios_QuestionarioId",
                        column: x => x.QuestionarioId,
                        principalTable: "Questionarios",
                        principalColumn: "QuestionarioId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Respostas",
                columns: table => new
                {
                    RespostaId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    QuestaoId = table.Column<int>(nullable: false),
                    Valor = table.Column<double>(nullable: false),
                    Observacao = table.Column<string>(type: "text", nullable: true),
                    QuestaoConteudo = table.Column<string>(type: "text", nullable: true),
                    AvaliacaoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Respostas", x => x.RespostaId);
                    table.ForeignKey(
                        name: "FK_Respostas_Avaliacoes_AvaliacaoId",
                        column: x => x.AvaliacaoId,
                        principalTable: "Avaliacoes",
                        principalColumn: "AvaliacaoId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Respostas_Questoes_QuestaoId",
                        column: x => x.QuestaoId,
                        principalTable: "Questoes",
                        principalColumn: "QuestaoId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Avaliacoes_AppUserId",
                table: "Avaliacoes",
                column: "AppUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Avaliacoes_AuditorId",
                table: "Avaliacoes",
                column: "AuditorId");

            migrationBuilder.CreateIndex(
                name: "IX_Avaliacoes_ProdutoId",
                table: "Avaliacoes",
                column: "ProdutoId");

            migrationBuilder.CreateIndex(
                name: "IX_Avaliacoes_QuestionarioId",
                table: "Avaliacoes",
                column: "QuestionarioId");

            migrationBuilder.CreateIndex(
                name: "IX_Respostas_AvaliacaoId",
                table: "Respostas",
                column: "AvaliacaoId");

            migrationBuilder.CreateIndex(
                name: "IX_Respostas_QuestaoId",
                table: "Respostas",
                column: "QuestaoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Respostas");

            migrationBuilder.DropTable(
                name: "Avaliacoes");
        }
    }
}
