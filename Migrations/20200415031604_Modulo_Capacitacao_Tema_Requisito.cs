﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace desempenho_cge.Migrations
{
    public partial class Modulo_Capacitacao_Tema_Requisito : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Requisito",
                table: "Capacitacoes",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Tema",
                table: "Capacitacoes",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Requisito",
                table: "Capacitacoes");

            migrationBuilder.DropColumn(
                name: "Tema",
                table: "Capacitacoes");
        }
    }
}
