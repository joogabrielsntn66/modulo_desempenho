﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace desempenho_cge.Migrations
{
    public partial class Modulo_Qualidade_Questionario_Questao : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Questionarios",
                columns: table => new
                {
                    QuestionarioId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(type: "varchar(200)", nullable: false),
                    Descricao = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Questionarios", x => x.QuestionarioId);
                });

            migrationBuilder.CreateTable(
                name: "Questoes",
                columns: table => new
                {
                    QuestaoId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Conteudo = table.Column<string>(type: "text", nullable: false),
                    Nivel = table.Column<int>(nullable: false),
                    QuestaoPaiId = table.Column<int>(nullable: true),
                    QuestionarioId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Questoes", x => x.QuestaoId);
                    table.ForeignKey(
                        name: "FK_Questoes_Questoes_QuestaoPaiId",
                        column: x => x.QuestaoPaiId,
                        principalTable: "Questoes",
                        principalColumn: "QuestaoId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Questoes_Questionarios_QuestionarioId",
                        column: x => x.QuestionarioId,
                        principalTable: "Questionarios",
                        principalColumn: "QuestionarioId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Questoes_QuestaoPaiId",
                table: "Questoes",
                column: "QuestaoPaiId");

            migrationBuilder.CreateIndex(
                name: "IX_Questoes_QuestionarioId",
                table: "Questoes",
                column: "QuestionarioId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Questoes");

            migrationBuilder.DropTable(
                name: "Questionarios");
        }
    }
}
