﻿using desempenho_cge.Models;
using desempenho_cge.Models.Account;
using desempenho_cge.Repositories;
using desempenho_cge.ViewModels;
using desempenho_cge.ViewModels.Qualidade;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;

namespace desempenho_cge.Controllers
{
    public class AvaliacaoController : Controller
    {
        private UserManager<AppUser> userManager;
        private readonly AvaliacaoRepository repository;
        private readonly AuditorRepository auditorRepository;
        private readonly ProdutoRepository produtoRepository;
        private readonly QuestionarioRepository questionarioRepository;
        private readonly RespostaRepository respostaRepository;

        public AvaliacaoController(
            UserManager<AppUser> usrMgr,
            AvaliacaoRepository _repository,
            AuditorRepository _auditorRepository,
            ProdutoRepository _produtoRepository,
            QuestionarioRepository _questionarioRepository,
            RespostaRepository _respostaRepository)
        {
            userManager = usrMgr;
            repository = _repository;
            auditorRepository = _auditorRepository;
            produtoRepository = _produtoRepository;
            questionarioRepository = _questionarioRepository;
            respostaRepository = _respostaRepository;
        }

        [HttpGet]
        [Authorize(Roles = "Administrador")]
        [Route("avaliacao/todas")]
        public IActionResult Index() => View(repository.GetAll());

        [HttpGet]
        [Authorize(Roles = "Administrador")]
        [Route("avaliacao/criar")]
        public IActionResult Create()
        {
            ViewBag.Questionarios = new SelectList(
                questionarioRepository.GetAll(), "QuestionarioId", "Nome");

            ViewBag.Auditores = new SelectList(
                auditorRepository.GetAll(), "AuditorId", "Nome");

            ViewBag.Produtos = new SelectList(
                produtoRepository.GetAll(), "ProdutoId", "Nome");

            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("avaliacao/criar")]
        [ValidateAntiForgeryToken]
        public IActionResult Create(AvaliacaoViewModel avaliacaoView)
        {
            if(ModelState.IsValid)
            {
                var avaliacao = repository.Converter(avaliacaoView);
                if(User.Identity.IsAuthenticated)
                {
                    avaliacao.AppUserId = userManager.GetUserId(User);
                }
                repository.Save(avaliacao);
                return RedirectToAction("Index");
            }

            ViewBag.Messages = new[] {
                new AlertViewModel("danger", "Algo de errado aconteceu!", "Os dados não foram inseridos corretamente")
            };

            ViewBag.Questionarios = new SelectList(
                questionarioRepository.GetAll(), "QuestionarioId", "Nome");

            ViewBag.Auditores = new SelectList(
                auditorRepository.GetAll(), "AuditorId", "Nome");

            ViewBag.Produtos = new SelectList(
                produtoRepository.GetAll(), "ProdutoId", "Nome");

            return View(avaliacaoView);
        }

        [HttpGet]
        [Authorize(Roles = "Administrador")]
        [Route("avaliacao/editar/{id}")]
        public IActionResult Edit(int id)
        {
            var avaliacao = repository.GetById(id);

            if (avaliacao == null)
                return NotFound();

            ViewBag.Questionarios = new SelectList(
                questionarioRepository.GetAll(), "QuestionarioId", "Nome", avaliacao.QuestionarioId);

            ViewBag.Auditores = new SelectList(
                auditorRepository.GetAll(), "AuditorId", "Nome", avaliacao.AuditorId);

            ViewBag.Produtos = new SelectList(
                produtoRepository.GetAll(), "ProdutoId", "Nome", avaliacao.ProdutoId);

            return View(repository.Converter(avaliacao));
        }

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("avaliacao/editar/{id}")]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, AvaliacaoViewModel avaliacaoView)
        {
            var avaliacao = repository.GetById(id);            
            if(ModelState.IsValid && avaliacao != null)
            {
                string appUserId = avaliacao.AppUserId;
                avaliacao = repository.Converter(avaliacaoView);
                avaliacao.AppUserId = appUserId;
                repository.Save(avaliacao);
                return RedirectToAction("Index");
            }

            ViewBag.Messages = new[] {
                new AlertViewModel("danger", "Algo de errado aconteceu!", "Os dados não foram inseridos corretamente")
            };

            ViewBag.Questionarios = new SelectList(
                questionarioRepository.GetAll(), "QuestionarioId", "Nome", avaliacaoView.QuestionarioId);

            ViewBag.Auditores = new SelectList(
                auditorRepository.GetAll(), "AuditorId", "Nome", avaliacaoView.AuditorId);

            ViewBag.Produtos = new SelectList(
                produtoRepository.GetAll(), "ProdutoId", "Nome", avaliacaoView.ProdutoId);

            return View(avaliacaoView);
        }

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("avaliacao/excluir/{id}")]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int id)
        {
            var avaliacao = repository.Exist(id);
            if (!avaliacao)
                return NotFound();
            repository.Delete(id);
            return RedirectToAction("Index");
        }

        [HttpGet]
        [Authorize(Roles = "Administrador")]
        [Route("avaliacao/responder/{id}")]
        public IActionResult Answers(int id)
        {
            var avaliacao = repository.GetById(id);            
            if(avaliacao != null)
            {
                var avaliacaoResposta = new AvaliacaoRespostasViewModel() { AvaliacaoId = avaliacao.AvaliacaoId };
                avaliacao.Questionario = questionarioRepository.GetById(avaliacao.QuestionarioId);

                if (!respostaRepository.GetAll(avaliacao.AvaliacaoId).Any())
                    repository.CreateAnswers(avaliacao);

                var respostas = respostaRepository.GetAll(avaliacao.AvaliacaoId);
                avaliacaoResposta.Grupos = GetGrupos(avaliacao.Questionario, respostas);

                return View(avaliacaoResposta);
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public JsonResult Responder(int AvaliacaoId, List<Resposta> RespostasView)
        {
            var respostas = respostaRepository.GetAll(AvaliacaoId);

            if(RespostasView != null || RespostasView.Count > 0)
            {
                foreach(var respostaView in RespostasView)
                {
                    int index = respostas.FindIndex(x => x.RespostaId == respostaView.RespostaId);
                    if(index != -1)
                    {
                        respostas[index].Valor = respostaView.Valor;
                        respostas[index].Observacao = respostaView.Observacao;
                    }
                }

                respostaRepository.CalcularPontuacao(respostas);
                foreach (var resposta in respostas)
                {
                    resposta.Questao = null;
                    respostaRepository.Save(resposta);
                }

                return Json(true);
            }

            return Json(false);
        }

        private List<GrupoRespostasViewModel> GetGrupos(Questionario questionario, List<Resposta> respostas)
        {
            List<GrupoRespostasViewModel> gruposView = new List<GrupoRespostasViewModel>();

            var grupos = questionario.Grupos.ToList();
            foreach(var grupo in grupos)
            {
                var grupoView = new GrupoRespostasViewModel() 
                {
                    GrupoId = grupo.QuestaoId,
                    Conteudo = grupo.Conteudo,
                };
                
                foreach(var questao in grupo.SubQuestoes)
                {
                    var resposta = respostas.Where(x => x.QuestaoId == questao.QuestaoId).FirstOrDefault();                    
                    grupoView.Respostas.Add(resposta);

                    var subquestoes = questao.SubQuestoes.ToList();
                    subquestoes
                        .ForEach(x => grupoView.Respostas.Add(
                                respostas.Where(q => q.QuestaoId == x.QuestaoId).FirstOrDefault()
                            ));
                }

                gruposView.Add(grupoView);
            }

            return gruposView.OrderBy(x => x.GrupoId).ToList();
        }
    }
}