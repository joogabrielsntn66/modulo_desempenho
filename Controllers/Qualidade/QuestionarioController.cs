﻿using desempenho_cge.Models;
using desempenho_cge.Repositories;
using desempenho_cge.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace desempenho_cge.Controllers
{
    public class QuestionarioController : Controller
    {
        private readonly QuestionarioRepository repository;

        public QuestionarioController(QuestionarioRepository _repository)
        {
            repository = _repository;
        }

        [HttpGet]
        [Authorize(Roles = "Administrador")]
        [Route("questionario/todos")]
        public IActionResult Index() => View(repository.GetAll());

        [HttpGet]
        [Authorize(Roles = "Administrador")]
        [Route("questionario/criar")]
        public IActionResult Create() => View();

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("questionario/criar")]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Questionario questionarioView)
        {
            if(questionarioView.Nome != null || questionarioView.Nome != string.Empty)
            {
                repository.Save(questionarioView);
                return RedirectToAction("Index");
            }

            ViewBag.Messages = new[] {
                new AlertViewModel("danger", "Algo de errado aconteceu!", "Os dados não foram inseridos corretamente")
            };

            return View(questionarioView);
        }

        [HttpGet]
        [Authorize(Roles = "Administrador")]
        [Route("questionario/editar/{id}")]
        public IActionResult Edit(int id) => View(repository.GetById(id));

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("questionario/editar/{id}")]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, Questionario questionarioView)
        {
            var questionario = repository.GetById(id);

            if (questionario != null && questionarioView.Nome != null || questionarioView.Nome != string.Empty)
            {
                questionario.Nome = questionarioView.Nome;
                questionario.Descricao = questionarioView.Descricao;
                repository.Save(questionario);
                return RedirectToAction("Index");
            }

            ViewBag.Messages = new[] {
                new AlertViewModel("danger", "Algo de errado aconteceu!", "Os dados não foram inseridos corretamente")
            };

            return View(questionarioView);
        }

        [HttpPost]
        [Route("questionario/excluir/{id}")]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int id)
        {
            var questionario = repository.Exist(id);
            if (!questionario) return NotFound();
            repository.Delete(id);
            return RedirectToAction("Index");
        }
    }
}