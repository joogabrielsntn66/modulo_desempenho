﻿using desempenho_cge.Models;
using desempenho_cge.Repositories;
using desempenho_cge.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace desempenho_cge.Controllers
{
    public class GrupoController : Controller
    {
        private readonly QuestaoRepository repository;

        public GrupoController(QuestaoRepository _repository)
        {
            repository = _repository;
        }

        [HttpGet]
        [Authorize(Roles = "Administrador")]
        [Route("grupo/informacoes/{id}")]
        public IActionResult Index(int id) => View(repository.GetById(id));

        [HttpGet]
        [Authorize(Roles = "Administrador")]
        [Route("grupo/criar/{id}")]
        public IActionResult Create(int id)
        {
            Questao grupo = new Questao
            {
                Nivel = 0,
                QuestionarioId = id
            };
            return View(grupo);
        }

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("grupo/criar/{id}")]
        [ValidateAntiForgeryToken]
        public IActionResult Create(int id, Questao grupoView)
        {
            if(grupoView.Conteudo != null || grupoView.Conteudo != string.Empty)
            {
                repository.Save(grupoView);
                return RedirectToAction("Index", new { id });
            }

            ViewBag.Messages = new[] {
                new AlertViewModel("danger", "Algo de errado aconteceu!", "Os dados não foram inseridos corretamente")
            };

            return View(grupoView);
        }

        [HttpGet]
        [Authorize(Roles = "Administrador")]
        [Route("grupo/editar/{id}")]
        public IActionResult Edit(int id) => View(repository.GetById(id));

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("grupo/editar/{id}")]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, Questao grupoView)
        {
            var grupo = repository.GetById(id);
            
            if(grupo != null && grupoView.Conteudo != null || grupoView.Conteudo != string.Empty)
            {
                grupo.Conteudo = grupoView.Conteudo;
                repository.Save(grupo);
                return RedirectToAction("Index", new { id });
            }


            ViewBag.Messages = new[] {
                new AlertViewModel("danger", "Algo de errado aconteceu!", "Os dados não foram inseridos corretamente")
            };

            return View(grupoView);
        }

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("grupo/excluir/{id}")]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int id)
        {
            var grupo = repository.Exist(id);
            if (!grupo)
                return NotFound();
            repository.Delete(id);
            return RedirectToAction("Index", "Questionario");
        }
    }
}