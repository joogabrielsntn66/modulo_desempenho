﻿using desempenho_cge.Models;
using desempenho_cge.Repositories;
using desempenho_cge.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace desempenho_cge.Controllers
{
    public class QuestaoController : Controller
    {
        private readonly QuestaoRepository repository;

        public QuestaoController(QuestaoRepository _repository)
        {
            repository = _repository;
        }

        [HttpGet]
        [Authorize(Roles = "Administrador")]
        [Route("questao/criar/{id}")]
        public IActionResult Create(int id)
        {
            var questaoPai = repository.GetById(id);

            if (questaoPai.Nivel == 0)
                ViewBag.GrupoId = questaoPai.QuestaoId;
            else
                ViewBag.GrupoId = questaoPai.QuestaoPaiId;

            var questaoFilha = new Questao
            {
                Nivel = questaoPai.Nivel + 1,
                QuestaoPai = questaoPai,
                QuestaoPaiId = questaoPai.QuestaoId
            };

            return View(questaoFilha);
        }

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("questao/criar/{id}")]
        [ValidateAntiForgeryToken]
        public IActionResult Create(int id, Questao questaoView)
        {
            if(questaoView.Conteudo != null || questaoView.Conteudo != string.Empty)
            {
                var questaoPai = repository.GetById((int)questaoView.QuestaoPaiId);
                repository.Save(questaoView);
                if (questaoPai.Nivel == 0)
                    return RedirectToAction("Index", "Grupo", new { id });
                else
                    return RedirectToAction("Index", "Grupo", new { id = questaoPai.QuestaoPaiId });
            }
            
            ViewBag.Messages = new[] {
                new AlertViewModel("danger", "Algo de errado aconteceu!", "Os dados não foram inseridos corretamente")
            };

            return View(questaoView);
        }

        [HttpGet]
        [Authorize(Roles = "Administrador")]
        [Route("questao/editar/{id}")]
        public IActionResult Edit(int id)
        {
            var questao = repository.GetById(id);

            if (questao.QuestaoPai.Nivel == 0)
                ViewBag.GrupoId = questao.QuestaoPai.QuestaoId;
            else
                ViewBag.GrupoId = questao.QuestaoPai.QuestaoPaiId;

            return View(questao);
        }

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("questao/editar/{id}")]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, Questao questaoView)
        {
            var questao = repository.GetById(id);
            
            if(questao != null)
            {
                var questaoPai = repository.GetById((int)questao.QuestaoPaiId);
                questao.Conteudo = questaoView.Conteudo;
                repository.Save(questao);
                if (questaoPai.Nivel == 0)
                    return RedirectToAction("Index", "Grupo", new { id = questaoPai.QuestaoId });
                else
                    return RedirectToAction("Index", "Grupo", new { id = questaoPai.QuestaoPaiId });
            }

            ViewBag.Messages = new[] {
                new AlertViewModel("danger", "Algo de errado aconteceu!", "Os dados não foram inseridos corretamente")
            };

            return View(questaoView);
        }

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("questao/excluir/{id}")]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int id)
        {
            var questao = repository.GetById(id);
            if (questao == null)
                return NotFound();
            var questaoPai = repository.GetById((int)questao.QuestaoPaiId);
            repository.Delete(id);
            if (questaoPai.Nivel == 0)
                return RedirectToAction("Index", "Grupo", new { id = questaoPai.QuestaoId });
            else
                return RedirectToAction("Index", "Grupo", new { id = questaoPai.QuestaoPaiId });
        }
    }
}