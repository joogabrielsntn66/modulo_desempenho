﻿using desempenho_cge.Repositories;
using desempenho_cge.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace desempenho_cge.Controllers
{
    [Authorize]
    public class ModalidadesController : Controller
    {
        private readonly ModalidadeRepository repository;

        public ModalidadesController(ModalidadeRepository _repository)
        {
            repository = _repository;
        }

        [HttpGet]
        [Authorize]
        [Route("admin/modalidade/todas")]
        public IActionResult Index() => View(repository.GetAll());

        [HttpGet]
        [Authorize]
        [Route("admin/modalidade/detalhes/{id}")]
        public IActionResult Details(int id) => View(repository.GetById(id));

        [HttpGet]
        [Authorize(Roles = "Administrador")]
        [Route("admin/modalidade/criar")]
        public IActionResult Create() => View();

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("admin/modalidade/criar")]
        [ValidateAntiForgeryToken]
        public IActionResult Create(ModalidadeViewModel modalidadeView)
        {
            if(ModelState.IsValid)
            {
                var modalidade = repository.Converter(modalidadeView);
                repository.Save(modalidade);
                return RedirectToAction("Index");
            }

            ViewBag.Messages = new[] {
                new AlertViewModel("danger", "Algo de errado aconteceu!", "Os dados não foram inseridos corretamente")
            };

            return View(modalidadeView);
        }

        [HttpGet]
        [Authorize(Roles = "Administrador")]
        [Route("admin/modalidade/editar/{id}")]
        public IActionResult Edit(int id)
        {
            var modalidade = repository.GetById(id);
            if (modalidade == null)
                return NotFound();

            return View(repository.Converter(modalidade));
        }

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("admin/modalidade/editar/{id}")]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, ModalidadeViewModel modalidadeView)
        {
            var modalidade = repository.GetById(id);
            if(ModelState.IsValid && modalidade != null)
            {
                modalidade = repository.Converter(modalidadeView);
                repository.Save(modalidade);
                return RedirectToAction("Index");
            }

            ViewBag.Messages = new[] {
                new AlertViewModel("danger", "Algo de errado aconteceu!", "Os dados não foram inseridos corretamente")
            };

            return View(modalidadeView);
        }

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("admin/modalidade/excluir/{id}")]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int id)
        {
            var modalidade = repository.ModalidadeExist(id);

            if (!modalidade)
                return NotFound();

            repository.Delete(id);
            return RedirectToAction("Index");
        }
    }
}