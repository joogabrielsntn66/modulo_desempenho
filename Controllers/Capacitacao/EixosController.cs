﻿using System.Collections.Generic;
using desempenho_cge.Repositories;
using desempenho_cge.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace desempenho_cge.Controllers
{
    [Authorize]
    public class EixosController : Controller
    {
        private readonly EixoRepository repository;

        public EixosController(EixoRepository eixoRepository)
        {
            repository = eixoRepository;
        }

        [HttpGet]
        [Authorize]
        [Route("admin/eixo/todos")]
        public IActionResult Index() => View(repository.GetAll());

        [HttpGet]
        [Authorize]
        [Route("admin/eixo/detalhes/{id}")]
        public IActionResult Details(int id) => View(repository.GetById(id));

        [HttpGet]
        [Authorize(Roles = "Administrador")]
        [Route("admin/eixo/criar")]
        public IActionResult Create() => View();

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("admin/eixo/criar")]
        [ValidateAntiForgeryToken]
        public IActionResult Create(EixoViewModel eixoView)
        {
            if(ModelState.IsValid)
            {
                var eixo = repository.Converter(eixoView);
                repository.Save(eixo);
                return RedirectToAction("Index");
            }

            ViewBag.Messages = new[] {
                new AlertViewModel("danger", "Algo de errado aconteceu!", "Os dados não foram inseridos corretamente")
            };

            return View(eixoView);
        }

        [HttpGet]
        [Authorize(Roles = "Administrador")]
        [Route("admin/eixo/editar/{id}")]
        public IActionResult Edit(int id)
        {
            var eixo = repository.GetById(id);
            if (eixo == null)
                return NotFound();

            return View(repository.Converter(eixo));
        }

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("admin/eixo/editar/{id}")]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, EixoViewModel eixoView)
        {
            var eixo = repository.GetById(id);

            if(ModelState.IsValid && eixo != null)
            {
                eixo.Nome = eixoView.Nome;
                repository.Save(eixo);
                return RedirectToAction("Index");
            }

            ViewBag.Messages = new[] {
                new AlertViewModel("danger", "Algo de errado aconteceu!", "Os dados não foram inseridos corretamente")
            };

            return View(eixoView);
        }

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("admin/eixo/excluir/{id}")]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int id)
        {
            var eixo = repository.EixoExist(id);

            if (!eixo)
                return NotFound();

            repository.Delete(id);
            return RedirectToAction("Index");
        }
    }
}