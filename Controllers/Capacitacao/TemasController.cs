﻿using desempenho_cge.Repositories;
using desempenho_cge.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace desempenho_cge.Controllers
{
    [Authorize]
    public class TemasController : Controller
    {
        private readonly TemaRepository repository;
        private readonly EixoRepository eixoRepository;

        public TemasController(
            TemaRepository _repository,
            EixoRepository _eixoRepository)
        {
            repository = _repository;
            eixoRepository = _eixoRepository;
        }

        [HttpGet]
        [Authorize]
        [Route("admin/tema/todos")]
        public IActionResult Index() => View(repository.GetAll());

        [HttpGet]
        [Authorize]
        [Route("admin/tema/detalhes/{id}")]
        public IActionResult Details(int id) => View(repository.GetById(id));

        [HttpGet]
        [Authorize(Roles = "Administrador")]
        [Route("admin/tema/criar")]
        public IActionResult Create()
        {
            var eixos = eixoRepository.GetAll();
            ViewBag.Eixos = new SelectList(eixos, "EixoId", "Nome");
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("admin/tema/criar")]
        [ValidateAntiForgeryToken]
        public IActionResult Create(TemaViewModel temaView)
        {
            if(ModelState.IsValid)
            {
                var eixo = repository.Converter(temaView);
                repository.Save(eixo);
                return RedirectToAction("Index");
            }

            var eixos = eixoRepository.GetAll();
            ViewBag.Eixos = new SelectList(eixos, "EixoId", "Nome", temaView.EixoId);

            return View(temaView);
        }

        [HttpGet]
        [Authorize(Roles = "Administrador")]
        [Route("admin/tema/editar/{id}")]
        public IActionResult Edit(int id)
        {
            var tema = repository.GetById(id);
            var temaView = repository.Converter(tema);
            var eixos = eixoRepository.GetAll();
            ViewBag.Eixos = new SelectList(eixos, "EixoId", "Nome", temaView.EixoId);
            return View(temaView);
        }

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("admin/tema/editar/{id}")]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, TemaViewModel temaView)
        {
            var tema = repository.GetById(id);

            if(ModelState.IsValid && tema != null)
            {
                tema = repository.Converter(temaView);
                repository.Save(tema);
                return RedirectToAction("Index");
            }

            var eixos = eixoRepository.GetAll();
            ViewBag.Eixos = new SelectList(eixos, "EixoId", "Nome", temaView.EixoId);

            return View(temaView);
        }

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("admin/tema/excluir/{id}")]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int id)
        {
            var tema = repository.TemaExist(id);

            if (!tema)
                return NotFound();

            repository.Delete(id);

            return RedirectToAction("Index");
        }
    }
}