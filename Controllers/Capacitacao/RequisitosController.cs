﻿using desempenho_cge.Repositories;
using desempenho_cge.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace desempenho_cge.Controllers
{
    [Authorize]
    public class RequisitosController : Controller
    {
        private readonly RequisitoRepository repository;
        private readonly ModalidadeRepository modalidadeRepository;

        public RequisitosController(
            RequisitoRepository _repository,
            ModalidadeRepository _modalidadeRepository)
        {
            repository = _repository;
            modalidadeRepository = _modalidadeRepository;
        }

        [HttpGet]
        [Authorize]
        [Route("admin/requisito/todos")]
        public IActionResult Index() => View(repository.GetAll());

        [HttpGet]
        [Authorize]
        [Route("admin/requisito/detalhes/{id}")]
        public IActionResult Details(int id) => View(repository.GetById(id));

        [HttpGet]
        [Authorize(Roles = "Administrador")]
        [Route("admin/requisito/criar")]
        public IActionResult Create()
        {
            var modalidades = modalidadeRepository.GetAll();
            ViewBag.Modalidades = new SelectList(modalidades, "ModalidadeId", "Nome");
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("admin/requisito/criar")]
        [ValidateAntiForgeryToken]
        public IActionResult Create(RequisitoViewModel requisitoView)
        {
            if(ModelState.IsValid)
            {
                var requisito = repository.Converter(requisitoView);
                repository.Save(requisito);
                return RedirectToAction("Index");
            }

            var modalidades = modalidadeRepository.GetAll();
            ViewBag.Modalidades = new SelectList(modalidades, "ModalidadeId", "Nome", requisitoView.ModalidadeId);

            ViewBag.Messages = new[] {
                new AlertViewModel("danger", "Algo de errado aconteceu!", "Os dados não foram inseridos corretamente")
            };

            return View(requisitoView);
        }

        [HttpGet]
        [Authorize(Roles = "Administrador")]
        [Route("admin/requisito/editar/{id}")]
        public IActionResult Edit(int id)
        {
            var requisito = repository.GetById(id);
            if (requisito == null)
                return NotFound();

            var modalidades = modalidadeRepository.GetAll();
            ViewBag.Modalidades = new SelectList(modalidades, "ModalidadeId", "Nome", requisito.ModalidadeId);
            return View(repository.Converter(requisito));
        }

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("admin/requisito/editar/{id}")]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, RequisitoViewModel requisitoView)
        {
            var requisito = repository.GetById(id);
            if(ModelState.IsValid && requisito != null)
            {
                requisito = repository.Converter(requisitoView);
                repository.Save(requisito);
                return RedirectToAction("Index");
            }

            var modalidades = modalidadeRepository.GetAll();
            ViewBag.Modalidades = new SelectList(modalidades, "ModalidadeId", "Nome", requisitoView.ModalidadeId);

            ViewBag.Messages = new[] {
                new AlertViewModel("danger", "Algo de errado aconteceu!", "Os dados não foram inseridos corretamente")
            };

            return View(requisitoView);
        }

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("admin/requisito/excluir/{id}")]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int id)
        {
            var requisito = repository.RequisitoExist(id);

            if (!requisito)
                return NotFound();

            repository.Delete(id);
            return RedirectToAction("Index");
        }
    }
}