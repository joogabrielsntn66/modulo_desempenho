﻿using desempenho_cge.Models;
using desempenho_cge.Repositories;
using desempenho_cge.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.IO;
using System.Linq;

namespace desempenho_cge.Controllers
{
    [Authorize]
    public class CapacitacoesController : Controller
    {
        private readonly CapacitacaoRepository repository;
        private readonly ModalidadeRepository modalidadeRepository;
        private readonly AuditorRepository auditorRepository;
        private readonly EixoRepository eixoRepository;
        private readonly RequisitoRepository requisitoRepository;
        private readonly TemaRepository temaRepository;
        private readonly IWebHostEnvironment env;

        public CapacitacoesController(
            CapacitacaoRepository _repository,
            ModalidadeRepository _modalidadeRepository,
            AuditorRepository _auditorRepository,
            EixoRepository _eixoRepository,
            TemaRepository _temaRepository,
            RequisitoRepository _requisitoRepository,
            IWebHostEnvironment _env)
        {
            repository = _repository;
            modalidadeRepository = _modalidadeRepository;
            auditorRepository = _auditorRepository;
            eixoRepository = _eixoRepository;
            temaRepository = _temaRepository;
            requisitoRepository = _requisitoRepository;
            env = _env;
        }

        [HttpGet]
        [Authorize]
        [Route("capacitacao/todas")]
        public IActionResult Index() => View(repository.GetAll());

        [HttpGet]
        [Authorize]
        [Route("capacitacao/detalhes/{id}")]
        public IActionResult Details(int id) => View(repository.GetById(id));

        [HttpGet]
        [Authorize]
        [Route("capacitacao/criar")]
        public IActionResult Create()
        {
            ViewBag.Modalidades = new SelectList(
                modalidadeRepository.GetAll(), "ModalidadeId", "Nome");

            ViewBag.Auditores = new SelectList(
                auditorRepository.GetAll(), "AuditorId", "Nome");

            ViewBag.Eixos = new SelectList(
                eixoRepository.GetAll(), "EixoId", "Nome");

            return View();
        }

        [HttpPost]
        [Authorize]
        [Route("capacitacao/criar")]
        [ValidateAntiForgeryToken]
        public IActionResult Create(CapacitacaoViewModel capacitacaoView)
        {
            if(ModelState.IsValid)
            {
                var capacitacao = repository.Converter(capacitacaoView);

                if(capacitacaoView.Documento != null)
                    SaveDocumento(capacitacaoView, capacitacao);

                repository.Save(capacitacao);
                return RedirectToAction("Index");
            }

            ViewBag.Modalidades = new SelectList(
                modalidadeRepository.GetAll(), "ModalidadeId", "Nome", capacitacaoView.ModalidadeId);

            ViewBag.Auditores = new SelectList(
                auditorRepository.GetAll(), "AuditorId", "Nome", capacitacaoView.AuditorId);

            ViewBag.Eixos = new SelectList(
                eixoRepository.GetAll(), "EixoId", "Nome", capacitacaoView.EixoId);

            ViewBag.Messages = new[] {
                new AlertViewModel("danger", "Algo de errado aconteceu!", "Os dados não foram inseridos corretamente")
            };

            return View(capacitacaoView);
        }

        [HttpGet]
        [Authorize]
        [Route("capacitacao/editar/{id}")]
        public IActionResult Edit(int id)
        {
            var capacitacao = repository.GetById(id);

            if (capacitacao == null)
                return NotFound();

            ViewBag.Modalidades = new SelectList(
                modalidadeRepository.GetAll(), "ModalidadeId", "Nome", capacitacao.ModalidadeId);

            ViewBag.Auditores = new SelectList(
                auditorRepository.GetAll(), "AuditorId", "Nome", capacitacao.AuditorId);

            ViewBag.Eixos = new SelectList(
                eixoRepository.GetAll(), "EixoId", "Nome", capacitacao.EixoId);


            return View(repository.Converter(capacitacao));
        }

        [HttpPost]
        [Authorize]
        [Route("capacitacao/editar/{id}")]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, CapacitacaoViewModel capacitacaoView)
        {
            var capacitacao = repository.GetById(id);
            if(ModelState.IsValid)
            {
                capacitacao = repository.Converter(capacitacaoView);

                if (capacitacaoView.Documento != null)
                    SaveDocumento(capacitacaoView, capacitacao);

                repository.Save(capacitacao);
                return RedirectToAction("Index");
            }

            ViewBag.Modalidades = new SelectList(
                modalidadeRepository.GetAll(), "ModalidadeId", "Nome", capacitacaoView.ModalidadeId);

            ViewBag.Auditores = new SelectList(
                auditorRepository.GetAll(), "AuditorId", "Nome", capacitacaoView.AuditorId);

            ViewBag.Eixos = new SelectList(
                eixoRepository.GetAll(), "EixoId", "Nome", capacitacaoView.EixoId);

            ViewBag.Messages = new[] {
                new AlertViewModel("danger", "Algo de errado aconteceu!", "Os dados não foram inseridos corretamente")
            };

            return View(capacitacaoView);
        }

        [HttpPost]
        [Authorize]
        [Route("capacitacao/excluir/{id}")]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int id)
        {
            var capacitacao = repository.Exist(id);

            if (!capacitacao)
                return NotFound();

            DeleteDocumento(repository
                .GetAll()
                .Where(x => x.CapacitacaoId == id)
                .Select(x => x.DocumentoPath)
                .FirstOrDefault());

            repository.Delete(id);
            return RedirectToAction("Index");
        }

        [HttpGet]
        [Authorize]
        [Route("capacitacao/download/{id}")]
        public IActionResult Download(int id)
        {
            var documentoPath = repository
                .GetAll()
                .Where(x => x.CapacitacaoId == id)
                .Select(x => x.DocumentoPath)
                .FirstOrDefault();

            var documentoName = Path.GetFileName(documentoPath);
            var extension = Path.GetExtension(documentoPath);
            string contentType = "";

            if (extension == ".pdf")
                contentType = "application/pdf";
            else if(extension == ".jpg")
                contentType = "image/jpeg";
            else if (extension == ".png")
                contentType = "image/png";

            if (documentoPath == null)
                return RedirectToAction("Index");

            FileStream arquivo = new FileStream(documentoPath, FileMode.Open);
            FileStreamResult download = new FileStreamResult(arquivo, contentType);
            download.FileDownloadName = documentoName;

            return download;
        }

        [HttpGet] 
        public JsonResult GetRequisitosFromModalidade(int ModalidadeId)
        {
            var requisitos = requisitoRepository.GetAll()
                .Where(x => x.ModalidadeId == ModalidadeId)
                .ToList();

            var requisitosList = new SelectList(
                requisitos, "Nome", "Nome");

            return Json(requisitosList);
        }

        [HttpGet]
        public JsonResult GetTemasFromEixo(int EixoId)
        {
            var temas = temaRepository.GetAll()
                .Where(x => x.EixoId == EixoId)
                .ToList();

            var temasList = new SelectList(
                temas, "Nome", "Nome");

            return Json(temasList);
        }

        [HttpGet]
        public JsonResult GetTema(string NomeTema)
        {
            var temaRelevancia = temaRepository.GetAll()
                .Where(x => x.Nome == NomeTema)
                .Select(x => x.Relevancia);

            return Json(temaRelevancia);
        }

        [HttpGet]
        public JsonResult GetModalidade(int ModalidadeId)
        {
            var modalidade = modalidadeRepository
                .GetAll()
                .Where(x => x.ModalidadeId == ModalidadeId)
                .Select(x => x.TipoModalidade);

            return Json(modalidade);
        }

        private void SaveDocumento(CapacitacaoViewModel capacitacaoView, Capacitacao capacitacao)
        {
            var documentoPathOriginal = repository
                .GetAll()
                .Where(x => x.CapacitacaoId == capacitacao.CapacitacaoId)
                .Select(x => x.DocumentoPath).FirstOrDefault();

            DeleteDocumento(documentoPathOriginal);

            var documentoNome = Path.GetFileName(capacitacaoView.Documento.FileName);
            var documentoPath = Path.Combine(env.WebRootPath, "capacitacoes", documentoNome);

            using (var documentoSteam = new FileStream(documentoPath, FileMode.Create))
            {
                capacitacaoView.Documento.CopyTo(documentoSteam);
            }
            capacitacao.DocumentoPath = documentoPath;
        }

        private void DeleteDocumento(string documentoPath)
        {
            if (documentoPath != null || documentoPath != string.Empty)
            {
                if (System.IO.File.Exists(documentoPath))
                    System.IO.File.Delete(documentoPath);
            }
        }
    }
}