﻿using System.Collections.Generic;
using desempenho_cge.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace desempenho_cge.Controllers.Relatorios
{
    public class AuditorProdutoController : Controller
    {
        private readonly ProdutividadeRepository produtividadeRepository;
        private readonly AuditorRepository auditorRepository;
        private readonly ProdutoRepository produtoRepository;

        public AuditorProdutoController(
            ProdutividadeRepository _produtividadeRepository,
            AuditorRepository _auditorRepository,
            ProdutoRepository _produtoRepository)
        {
            produtividadeRepository = _produtividadeRepository;
            auditorRepository = _auditorRepository;
            produtoRepository = _produtoRepository;
        }

        [HttpGet]
        [Authorize]
        [Route("relatorio/auditor_produto/gerar")]
        public IActionResult Show()
        {
            ViewBag.Auditores = new SelectList(auditorRepository.GetAll(), "AuditorId", "Nome");
            ViewBag.Produtos = new SelectList(produtoRepository.GetAll(), "ProdutoId", "Nome");
            return View("../Relatorios/AuditorProduto/Show");
        }

        [HttpGet]
        [Authorize]
        [Route("relatorio/auditor_produto/dados")]
        public IActionResult SendData(List<int> auditoresView, List<int> produtosView, string situacao)
        {
            var auditores = auditorRepository.GetAuditores(auditoresView);
            var produtos = produtoRepository.GetProdutos(produtosView);
            var relatorio = new List<dynamic>();

            foreach (var auditor in auditores)
            {
                double pontosTotalAuditor = 0.0;
                var item = new Dictionary<string, object>();
                item["Auditor(a)"] = auditor.Nome;

                var produtividades = produtividadeRepository.GetProdutividadesByAuditor(auditor.AuditorId, situacao);

                foreach (var produto in produtos)
                {
                    double pontos = 0.0;

                    foreach (var produtividade in produtividades)
                    {
                        if (produtividade.ProdutoId == produto.ProdutoId)
                            pontos += produtividade.Pontuacao;
                    }
                    pontosTotalAuditor += pontos;

                    item[produto.Nome] = pontos.ToString("n3");
                }

                item["Total de Pontos"] = pontosTotalAuditor.ToString("n3");
                item["Fator Avaliativo Produtividade"] = produtividadeRepository.CalcularFAP(pontosTotalAuditor).ToString("n3");

                relatorio.Add(item);
            }

            return Json(relatorio);
        }
    }
}