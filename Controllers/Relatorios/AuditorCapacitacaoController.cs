﻿using System.Collections.Generic;
using System.Linq;
using desempenho_cge.Models;
using desempenho_cge.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace desempenho_cge.Controllers.Relatorios
{
    public class AuditorCapacitacaoController : Controller
    {
        private readonly AuditorRepository auditorRepository;
        private readonly CapacitacaoRepository capacitacaoRepository;
        private readonly ModalidadeRepository modalidadeRepository;

        public AuditorCapacitacaoController(
            AuditorRepository _auditorRepository,
            CapacitacaoRepository _capacitacaoRepository,
            ModalidadeRepository _modalidadeRepository)
        {
            auditorRepository = _auditorRepository;
            capacitacaoRepository = _capacitacaoRepository;
            modalidadeRepository = _modalidadeRepository;
        }

        [HttpGet]
        [Authorize]
        [Route("relatorio/auditor_capacitacao/gerar")]
        public IActionResult Show()
        {
            ViewBag.Auditores = new SelectList(auditorRepository.GetAll(), "AuditorId", "Nome");
            return View("../Relatorios/AuditorCapacitacao/Show");
        }

        [HttpGet]
        [Authorize]
        [Route("relatorio/auditor_capacitacao/dados")]
        public IActionResult SendDataBody(List<int> auditoresView, string situacao)
        {
            // Pegando todos os auditores com base nos filtros do usuário
            var auditores = auditorRepository.GetAuditores(auditoresView);
            // Pegando todas as modalidades do banco de dados
            var modalidades = modalidadeRepository.GetAll();
            // Essa lista guardará as linhas da tabela
            var relatorio = new List<dynamic>();
            // Essa lista guardará as colunas de cima
            var headers = GetHeaders(modalidades);
            // Essa lista juntará tudo e mandará para a view
            var dados = new List<List<dynamic>>();

            foreach (var auditor in auditores)
            {
                // Variável que representa uma linha da tabela
                var item = new Dictionary<string, object>
                { ["Auditor(a)"] = auditor.Nome };
                double pontosTotal = 0.0;
                // Pegando as capacitações com base nos filtros do usuário
                var capacitacoes = capacitacaoRepository.GetCapacitacoes(auditor.AuditorId, situacao);

                foreach (var modalidade in modalidades)
                {
                    double pontosModalidade = 0.0;

                    // Se a modalidade tiver algum requisito
                    if(modalidade.Requisitos.Count > 0)
                    {
                        double pontosRequisitos = 0.0;
                        var requisitos = modalidade.Requisitos.ToList();

                        // Passeando pelos requisitos da modalidade
                        foreach (var requisito in requisitos)
                        {
                            // Somando os pontos de capacitação que conter o requisito
                            foreach (var capacitacao in capacitacoes)
                            {
                                if (capacitacao.Requisito == requisito.Nome)
                                    pontosRequisitos += capacitacao.Pontuacao;
                            }

                            // Somando os pontos para depois utilizar nos cálculos
                            pontosModalidade += pontosRequisitos;

                            item[requisito.Nome] = pontosRequisitos.ToString("n3");
                        }
                    }
                    else 
                    {
                        // Se a modalidade não tiver requisito (caso de "Certificação", "Instrutor e Palestrante" e "Participação em Eventos")
                        foreach (var capacitacao in capacitacoes)
                        {
                            if (capacitacao.ModalidadeId == modalidade.ModalidadeId)
                                pontosModalidade += capacitacao.Pontuacao;
                        }
                    }                  

                    // Se o tipo de modalidade for diferente de "Pontuação p/ Relevância"
                    // Não precisa exibir o total ajustado
                    if (modalidade.TipoModalidade != "Pontuação p/ Relevância")
                    {
                        // Somando todos os pontos para calcular o FAC
                        item[modalidade.Nome + "/Total"] = pontosModalidade.ToString("n3");
                        item[modalidade.Nome + "/Total Ajustado"] = capacitacaoRepository.CalcularTotalAjustado(pontosModalidade).ToString("n3");
                        pontosTotal += capacitacaoRepository.CalcularTotalAjustado(pontosModalidade);
                    } 
                    else 
                    {
                        item[modalidade.Nome + "/Total"] = pontosModalidade.ToString("n3");
                        pontosTotal += pontosModalidade;
                    }
                }

                // Aqui vai calcular o FAC
                item["Total de Pontos"] = pontosTotal.ToString("n3");
                item["Fator Avaliativo Capacitação"] = capacitacaoRepository.CalcularFAC(pontosTotal).ToString("n3");
                relatorio.Add(item);
            }

            // Adicionando as colunas de cima
            dados.Add(headers);
            // Adicionando as linhas
            dados.Add(relatorio);

            return Json(dados);
        }

        private List<dynamic> GetHeaders(List<Modalidade> modalidades)
        {
            // Lista que vai armazenar as colunas de cima
            var headers = new List<dynamic>();

            // Adicionando a coluna Auditor
            headers.Add(new Dictionary<string, object>
            {
                ["title"] = "Auditor(a)",
                ["field"] = "Auditor(a)",
                ["rowspan"] = 2,
                ["sortable"] = true,
                ["valign"] = "middle",
                ["align"] = "center"
            });

            // Adicionando as demais colunas
            foreach (var modalidade in modalidades)
            {
                // Variável pra indicar quantas colunas a colunas de cima abrangerá
                int quantidadeColunas = 0;
                // Se o tipo de modalidade for "Pontuação p/ Relevância", só abrangerá uma coluna
                if (modalidade.TipoModalidade == "Pontuação p/ Relevância")
                    quantidadeColunas = 1;
                else
                    // Senão, abrangerá a quantidade de requisitos mais 2 (colunas "Total" e "Total Ajustado")
                    quantidadeColunas = 2;

                // Para cada modalidade, uma coluna em cima
                var header = new Dictionary<string, object> { 
                    ["title"] = modalidade.Nome,
                    ["colspan"] = modalidade.Requisitos.Count + quantidadeColunas,
                    ["align"] = "center"
                };
                headers.Add(header);
            }

            // Adicionando as duas últimas colunas "Total de Pontos" e "Fator Avaliativo Capacitação"
            string[] colunas = { "Total de Pontos", "Fator Avaliativo Capacitação" };
            foreach (var i in colunas)
            {
                headers.Add(new Dictionary<string, object>
                {
                    ["title"] = i,
                    ["field"] = i,
                    ["rowspan"] = 2,
                    ["sortable"] = true,
                    ["valign"] = "middle",
                    ["align"] = "center"
                });
            }

            // Retorna as colunas de cima
            return headers;
        }
    }
}