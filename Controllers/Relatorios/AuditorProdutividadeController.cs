﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using desempenho_cge.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace desempenho_cge.Controllers.Relatorios
{
    public class AuditorProdutividadeController : Controller
    {
        private readonly AuditorRepository auditorRepository;
        private readonly ProdutividadeRepository produtividadeRepository;

        public AuditorProdutividadeController(
            AuditorRepository _auditorRepository,
            ProdutividadeRepository _produtividadeRepository)
        {
            auditorRepository = _auditorRepository;
            produtividadeRepository = _produtividadeRepository;
        }

        [HttpGet]
        [Authorize]
        [Route("relatorio/auditor_produtividade/gerar")]
        public IActionResult Show()
        {
            ViewBag.Auditores = new SelectList(auditorRepository.GetAll(), "AuditorId", "Nome");
            return View("../Relatorios/AuditorProdutividade/Show");
        }

        [HttpGet]
        [Authorize]
        [Route("relatorio/auditor_produtividade/dados")]
        public IActionResult SendData(List<int> auditoresView, string situacao)
        {
            var auditores = auditorRepository.GetAuditores(auditoresView);
            var meses = DateTimeFormatInfo.CurrentInfo.MonthNames.Where(x => x != "" || x != string.Empty).ToList();
            var relatorio = new List<dynamic>();

            foreach (var auditor in auditores)
            {
                double pontosTotalAuditor = 0.0;
                var item = new Dictionary<string, object>();
                item["Auditor(a)"] = auditor.Nome;

                var produtividades = produtividadeRepository.GetProdutividadesByAuditor(auditor.AuditorId, situacao);

                foreach (var mes in meses)
                {
                    double pontos = 0.0;

                    foreach (var produtividade in produtividades)
                    {
                        if (produtividade.DataInicio.ToString("MMMM") == mes)
                            pontos += produtividade.Pontuacao;
                    }
                    pontosTotalAuditor += pontos;

                    item[mes] = pontos.ToString("n3");
                }

                item["Produtividade"] = pontosTotalAuditor.ToString("n3");
                relatorio.Add(item);
            }

            return Json(relatorio);
        }
    }
}