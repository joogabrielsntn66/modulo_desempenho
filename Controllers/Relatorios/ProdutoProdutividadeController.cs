﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using desempenho_cge.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace desempenho_cge.Controllers.Relatorios
{
    public class ProdutoProdutividadeController : Controller
    {
        private readonly ProdutoRepository produtoRepository;
        private readonly ProdutividadeRepository produtividadeRepository;

        public ProdutoProdutividadeController(
            ProdutoRepository _produtoRepository,
            ProdutividadeRepository _produtividadeRepository)
        {
            produtividadeRepository = _produtividadeRepository;
            produtoRepository = _produtoRepository;
        }

        [HttpGet]
        [Authorize]
        [Route("relatorio/produto_produtividade/gerar")]
        public IActionResult Show()
        {
            ViewBag.Produtos = new SelectList(produtoRepository.GetAll(), "ProdutoId", "Nome");
            return View("../Relatorios/ProdutoProdutividade/Show");
        }

        [HttpGet]
        [Authorize]
        [Route("relatorio/produto_produtividade/dados")]
        public IActionResult SendData(List<int> produtosView, string situacao)
        {
            var produtos = produtoRepository.GetProdutos(produtosView);
            var relatorio = new List<dynamic>();
            var meses = DateTimeFormatInfo.CurrentInfo.MonthNames.Where(x => x != "" || x != string.Empty).ToList();

            foreach (var produto in produtos)
            {
                double pontosTotalProduto = 0.0;
                var item = new Dictionary<string, object>();
                item["Produto"] = produto.Nome;

                var produtividades = produtividadeRepository.GetProdutividadesByProduto(produto.ProdutoId, situacao);

                foreach (var mes in meses)
                {
                    double pontos = 0.0;

                    foreach (var produtividade in produtividades)
                    {
                        if (produtividade.DataInicio.ToString("MMMM") == mes)
                            pontos += produtividade.Pontuacao;
                    }
                    pontosTotalProduto += pontos;

                    item[mes] = pontos.ToString("n3");
                }

                item["Produtividade"] = pontosTotalProduto.ToString("n3");
                relatorio.Add(item);
            }

            return Json(relatorio);
        }
    }
}