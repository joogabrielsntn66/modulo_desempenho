﻿using desempenho_cge.Repositories;
using desempenho_cge.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace desempenho_cge.Controllers
{
    [Authorize]
    public class OrgaosController : Controller
    {
        private readonly OrgaoRepository repository;

        public OrgaosController(OrgaoRepository _repository)
        {
            repository = _repository;
        }

        [HttpGet]
        [Authorize]
        [Route("admin/uos/todas")]
        public ActionResult Index() => View(repository.GetAll());

        [HttpGet]
        [Authorize]
        [Route("admin/uos/detalhes/{id}")]
        public ActionResult Details(int id) => View(repository.GetById(id));

        [HttpGet]
        [Authorize(Roles = "Administrador")]
        [Route("admin/uos/criar")]
        public ActionResult Create() => View();
                
        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("admin/uos/criar")]
        [ValidateAntiForgeryToken]
        public ActionResult Create(OrgaoViewModel orgaoView)
        {
            if(ModelState.IsValid)
            {
                var orgao = repository.Converter(orgaoView);
                repository.Save(orgao);
                return RedirectToAction("Index");
            }

            ViewBag.Messages = new[] {
                new AlertViewModel("danger", "Algo de errado aconteceu!", "Os dados não foram inseridos corretamente")
            };

            return View(orgaoView);
        }

        [HttpGet]
        [Authorize(Roles = "Administrador")]
        [Route("admin/uos/editar/{id}")]
        public ActionResult Edit(int id)
        {
            var orgao = repository.GetById(id);

            if (orgao == null)
                return NotFound();

            var orgaoView = repository.Converter(orgao);
            return View(orgaoView);
        }
                
        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("admin/uos/editar/{id}")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, OrgaoViewModel orgaoView)
        {
            var orgao = repository.GetById(id);
            if (ModelState.IsValid && orgao != null)
            {
                orgao = repository.Converter(orgaoView);
                repository.Save(orgao);
                return RedirectToAction("Index");
            }

            ViewBag.Messages = new[] {
                new AlertViewModel("danger", "Algo de errado aconteceu!", "Os dados não foram inseridos corretamente")
            };

            return View(orgaoView);
        }

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("admin/uos/excluir/{id}")]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            var orgao = repository.OrgaoExist(id);
            if (!orgao)
                return NotFound();

            repository.Delete(id);
            return RedirectToAction("Index");
        }
    }
}