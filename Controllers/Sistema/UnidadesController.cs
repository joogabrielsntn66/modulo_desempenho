﻿using desempenho_cge.Repositories;
using desempenho_cge.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace desempenho_cge.Controllers
{
    [Authorize]
    public class UnidadesController : Controller
    {
        private readonly UnidadeRepository repository;

        public UnidadesController(UnidadeRepository _repository)
        {
            repository = _repository;
        }

        [HttpGet]
        [Authorize]
        [Route("admin/unidade/todas")]
        public ActionResult Index() => View(repository.GetAll());

        [HttpGet]
        [Authorize]
        [Route("admin/unidade/detalhes/{id}")]
        public ActionResult Details(int id) => View(repository.GetById(id));

        [HttpGet]
        [Authorize(Roles = "Administrador")]
        [Route("admin/unidade/criar")]
        public ActionResult Create() => View();

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("admin/unidade/criar")]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UnidadeViewModel unidadeView)
        {
            if(ModelState.IsValid)
            {
                var unidade = repository.Converter(unidadeView);
                repository.Save(unidade);
                return RedirectToAction("Index");
            }

            ViewBag.Messages = new[] {
                new AlertViewModel("danger", "Algo de errado aconteceu!", "Os dados não foram inseridos corretamente")
            };

            return View(unidadeView);
        }

        [HttpGet]
        [Authorize(Roles = "Administrador")]
        [Route("admin/editar/{id}")]
        public ActionResult Edit(int id)
        {
            var unidade = repository.GetById(id);

            if(unidade == null)
                return NotFound();

            var unidadeView = repository.Converter(unidade);
            return View(unidadeView);
        }
                
        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("admin/editar/{id}")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, UnidadeViewModel unidadeView)
        {
            var unidade = repository.GetById(id);

            if(ModelState.IsValid && unidade != null)
            {
                unidade = repository.Converter(unidadeView);
                repository.Save(unidade);
                return RedirectToAction("Index");
            }

            ViewBag.Messages = new[] {
                new AlertViewModel("danger", "Algo de errado aconteceu!", "Os dados não foram inseridos corretamente")
            };

            return View(unidadeView);
        }
                
        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("admin/excluir/{id}")]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int id)
        {
            var unidade = repository.UnidadeExist(id);
            if (!unidade)
                return NotFound();

            repository.Delete(id);                      
            return RedirectToAction("Index");
        }
    }
}