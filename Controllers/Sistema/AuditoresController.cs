﻿using desempenho_cge.Repositories;
using desempenho_cge.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace desempenho_cge.Controllers
{
    [Authorize]
    public class AuditoresController : Controller
    {
        private readonly AuditorRepository auditorRepository;
        private readonly UnidadeRepository unidadeRepository;

        public AuditoresController(
            AuditorRepository _auditorRepository, 
            UnidadeRepository _unidadeRepository)
        {
            auditorRepository = _auditorRepository;
            unidadeRepository = _unidadeRepository;
        }

        [HttpGet]
        [Authorize]
        [Route("admin/auditor/todos")]
        public ActionResult Index() => View(auditorRepository.GetAll());

        [HttpGet]
        [Authorize]
        [Route("admin/auditor/detalhes/{id}")]
        public ActionResult Details(int id) => View(auditorRepository.GetById(id));

        [HttpGet]
        [Authorize(Roles = "Administrador")]
        [Route("admin/auditor/criar")]
        public ActionResult Create()
        {
            var unidades = unidadeRepository.GetAll();
            ViewBag.Unidades = new SelectList(unidades, "UnidadeId", "Nome");
            return View();
        }
                
        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("admin/auditor/criar")]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AuditorViewModel auditorView)
        {
            if(ModelState.IsValid)
            {
                var auditor = auditorRepository.Converter(auditorView);
                auditorRepository.Save(auditor);
                return RedirectToAction("Index");
            }

            var unidades = unidadeRepository.GetAll();
            ViewBag.Unidades = new SelectList(unidades, "UnidadeId", "Nome", auditorView.UnidadeId);

            ViewBag.Messages = new[] {
                new AlertViewModel("danger", "Algo de errado aconteceu!", "Os dados não foram inseridos corretamente")
            };

            return View(auditorView);
        }

        [HttpGet]
        [Authorize(Roles = "Administrador")]
        [Route("admin/auditor/editar/{id}")]
        public ActionResult Edit(int id)
        {
            var auditor = auditorRepository.GetById(id);

            if (auditor == null)
                return NotFound();

            var unidades = unidadeRepository.GetAll();
            var auditorView = auditorRepository.Converter(auditor);
            ViewBag.Unidades = new SelectList(unidades, "UnidadeId", "Nome", auditorView.UnidadeId);
            return View(auditorView);
        }

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("admin/auditor/editar/{id}")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, AuditorViewModel auditorView)
        {
            var auditor = auditorRepository.GetById(id);

            if(ModelState.IsValid && auditor != null)
            {
                auditor = auditorRepository.Converter(auditorView);
                auditorRepository.Save(auditor);
                return RedirectToAction("Index");
            }

            var unidades = unidadeRepository.GetAll();
            ViewBag.Unidades = new SelectList(unidades, "UnidadeId", "Nome",auditorView.UnidadeId);

            ViewBag.Messages = new[] {
                new AlertViewModel("danger", "Algo de errado aconteceu!", "Os dados não foram inseridos corretamente")
            };

            return View(auditorView);
        }

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("admin/auditor/excluir/{id}")]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            var auditor = auditorRepository.AuditorExist(id);
            if (!auditor)
                return NotFound();

            auditorRepository.Delete(id);
            return RedirectToAction("Index");
        }
    }
}