﻿using System.Linq;
using System.Threading.Tasks;
using desempenho_cge.Models.Account;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace desempenho_cge.Controllers.Account
{
    [Authorize(Roles = "Administrador")]
    public class AdminController : Controller
    {
        private UserManager<AppUser> userManager;
        private IPasswordHasher<AppUser> passwordHasher;
        private IPasswordValidator<AppUser> passwordValidator;

        public AdminController(
            UserManager<AppUser> usrMgr,
            IPasswordHasher<AppUser> passwordHash,
            IPasswordValidator<AppUser> passwordVal)
        {
            userManager = usrMgr;
            passwordHasher = passwordHash;
            passwordValidator = passwordVal;
        }

        [HttpGet]
        [Authorize(Roles = "Administrador")]
        [Route("admin/usuario/todos")]
        public IActionResult Index() => View(userManager.Users);

        [HttpGet]
        [Authorize(Roles = "Administrador")]
        [Route("admin/usuario/criar")]
        public ViewResult Create() => View();

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("admin/usuario/criar")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(User user)
        {
            bool IsCpfExist = userManager.Users.Any(x => x.Cpf == user.Cpf);
            if(IsCpfExist == true)
            {
                ModelState.AddModelError("Cpf", "Esse CPF já existe no banco de dados");
            }

            if (ModelState.IsValid)
            {
                AppUser appUser = new AppUser
                {
                    Cpf = user.Cpf,
                    UserName = user.UserName,
                    Email = user.Email
                };

                IdentityResult result = await userManager.CreateAsync(appUser, user.Password);
                if (result.Succeeded)
                    return RedirectToAction("Index");
                else
                    Errors(result);
            }
            return View(user);
        }

        [HttpGet]
        [Authorize(Roles = "Administrador")]
        [Route("admin/usuario/editar/{id}")]
        public async Task<IActionResult> Update(string id)
        {
            AppUser user = await userManager.FindByIdAsync(id);
            if (user != null)
                return View(user);
            else
                return RedirectToAction("Index");
        }

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("admin/usuario/editar/{id}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(string id, string cpf, string email, string password)
        {

            AppUser user = await userManager.FindByIdAsync(id);
            if(user != null)
            {
                bool IsCpfExist = false;
                if (!string.IsNullOrEmpty(cpf))
                {
                    IsCpfExist = userManager.Users.Any(x => x.Cpf == cpf);
                    if (IsCpfExist == true)
                        ModelState.AddModelError("Cpf", "Esse C.P.F já existe no banco de dados");
                    else
                        user.Cpf = cpf;
                }
                else
                    ModelState.AddModelError("", "C.P.F não pode ser vazio");

                if (!string.IsNullOrEmpty(email))
                    user.Email = email;
                else
                    ModelState.AddModelError("", "Email não pode ser vazio");

                IdentityResult validPass = null;
                if (!string.IsNullOrEmpty(password))
                {
                    validPass = await passwordValidator.ValidateAsync(userManager, user, password);
                    if (validPass.Succeeded)
                        user.PasswordHash = passwordHasher.HashPassword(user, password);
                    else
                        Errors(validPass);
                }
                else
                    ModelState.AddModelError("", "Password não pode ser vazio");

                if (!string.IsNullOrEmpty(email) && validPass != null && validPass.Succeeded && IsCpfExist == false)
                {
                    IdentityResult result = await userManager.UpdateAsync(user);
                    if (result.Succeeded)
                        return RedirectToAction("Index");
                    else
                        Errors(result);
                }
            }
            else
                ModelState.AddModelError("", "Usuário não foi encontrado");
            return View(user);
        }

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("admin/usuario/excluir/{id}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(string id)
        {
            AppUser user = await userManager.FindByIdAsync(id);
            if (user != null)
            {
                IdentityResult result = await userManager.DeleteAsync(user);
                if (result.Succeeded)
                    return RedirectToAction("Index");
                else
                    Errors(result);
            }
            else
                ModelState.AddModelError("", "User Not Found");
            return View("Index", userManager.Users);
        }

        private void Errors(IdentityResult result)
        {
            foreach (IdentityError error in result.Errors)
                ModelState.AddModelError("", error.Description);
        }
    }
}