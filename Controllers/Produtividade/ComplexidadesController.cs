﻿using desempenho_cge.Repositories;
using desempenho_cge.ViewModels;
using desempenho_cge.ViewModels.Produtividade;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace desempenho_cge.Controllers
{
    [Authorize]
    public class ComplexidadesController : Controller
    {
        private readonly ComplexidadeRepository repository;

        public ComplexidadesController(ComplexidadeRepository _repository)
        {
            repository = _repository;
        }

        [HttpGet]
        [Authorize]
        [Route("admin/complexidade/todas")]
        public ActionResult Index() => View(repository.GetAll());

        [HttpGet]
        [Authorize]
        [Route("admin/complexidade/detalhes/{id}")]
        public ActionResult Details(int id) => View(repository.GetById(id));

        [HttpGet]
        [Authorize(Roles = "Administrador")]
        [Route("admin/complexidade/criar")]
        public ActionResult Create() => View();

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("admin/complexidade/criar")]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ComplexidadeViewModel complexidadeView)
        {
            if(ModelState.IsValid)
            {
                var complexidade = repository.Converter(complexidadeView);
                repository.Save(complexidade);
                return RedirectToAction(nameof(Index));
            }

            ViewBag.Messages = new[] {
                new AlertViewModel("danger", "Algo de errado aconteceu!", "Os dados não foram inseridos corretamente")
            };

            return View(complexidadeView);
        }

        [HttpGet]
        [Authorize(Roles = "Administrador")]
        [Route("admin/complexidade/editar/{id}")]
        public ActionResult Edit(int id)
        {
            var complexidade = repository.GetById(id);

            if (complexidade == null)
                return NotFound();

            var complexidadeView = repository.Converter(complexidade);
            return View(complexidadeView);
        }

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("admin/complexidade/editar/{id}")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, ComplexidadeViewModel complexidadeView)
        {
            var complexidade = repository.GetById(id);
            if(ModelState.IsValid && complexidade != null)
            {
                complexidade = repository.Converter(complexidadeView);
                repository.Save(complexidade);
                return RedirectToAction(nameof(Index));
            }

            ViewBag.Messages = new[] {
                new AlertViewModel("danger", "Algo de errado aconteceu!", "Os dados não foram inseridos corretamente")
            };

            return View(complexidadeView);
        }             

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("admin/complexidade/excluir/{id}")]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            var complexidade = repository.ComplexidadeExist(id);
            if (!complexidade)
                return NotFound();

            repository.Delete(id);
            return RedirectToAction("Index");
        }
    }
}