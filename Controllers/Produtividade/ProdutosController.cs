﻿using System.Collections.Generic;
using System.Linq;
using desempenho_cge.Models.Produtividade;
using desempenho_cge.Repositories;
using desempenho_cge.ViewModels;
using desempenho_cge.ViewModels.Items;
using desempenho_cge.ViewModels.Produtividade;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace desempenho_cge.Controllers
{
    [Authorize]
    public class ProdutosController : Controller
    {
        private readonly ProdutoRepository produtoRepository;
        private readonly ComplexidadeRepository complexidadeRepository;

        public ProdutosController(
            ProdutoRepository _produtoRepository,
            ComplexidadeRepository _complexidadeRepository)
        {
            produtoRepository = _produtoRepository;
            complexidadeRepository = _complexidadeRepository;
        }

        [HttpGet]
        [Authorize]
        [Route("admin/produto/todas")]
        public ActionResult Index() => View(produtoRepository.GetAll());

        [HttpGet]
        [Authorize]
        [Route("admin/produto /detalhes/{id}")]
        public ActionResult Details(int id) => View(produtoRepository.GetById(id));

        [HttpGet]
        [Authorize(Roles = "Administrador")]
        [Route("admin/produto/criar")]
        public ActionResult Create()
        {
            ProdutoViewModel produtoView = new ProdutoViewModel();

            var allComplexidades = complexidadeRepository.GetAll();
            var checkBoxitems = new List<CheckBoxListItem>();

            allComplexidades.
                ForEach(x => checkBoxitems.Add(new CheckBoxListItem() { 
                    Id = x.ComplexidadeId,
                    Display = x.Classificacao,
                    IsChecked = false
                }));

            produtoView.Complexidades = checkBoxitems;

            return View(produtoView);
        }

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("admin/produto/criar")]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProdutoViewModel produtoView)
        {
            if(ModelState.IsValid)
            {
                var complexidadesSelecionadas = produtoView.Complexidades
                   .Where(x => x.IsChecked)
                   .Select(x => x.Id).ToList();

                var produto = new Produto()
                {
                    Nome = produtoView.Nome,
                    Esforco = produtoView.Esforco,
                    PontoBase = (double) produtoView.Esforco / 8,
                    TipoPontuacao = produtoView.TipoPontuacao
                };

                produtoRepository.Save(produto, complexidadesSelecionadas);
                return RedirectToAction("Index");
            }

            var allComplexidades = complexidadeRepository.GetAll();
            var checkBoxitems = new List<CheckBoxListItem>();

            allComplexidades.
                ForEach(x => checkBoxitems.Add(new CheckBoxListItem()
                {
                    Id = x.ComplexidadeId,
                    Display = x.Classificacao,
                    IsChecked = false
                }));

            produtoView.Complexidades = checkBoxitems;

            ViewBag.Messages = new[] {
                new AlertViewModel("danger", "Algo de errado aconteceu!", "Os dados não foram inseridos corretamente")
            };

            return View(produtoView);
        }

        [HttpGet]
        [Authorize(Roles = "Administrador")]
        [Route("admin/produto/editar/{id}")]
        public ActionResult Edit(int id)
        {
            var produto = produtoRepository.GetById(id);

            if (produto == null)
                return NotFound();

            var produtoView = new ProdutoViewModel() { 
                ProdutoId = produto.ProdutoId,
                Nome = produto.Nome,
                Esforco = produto.Esforco,
                TipoPontuacao = produto.TipoPontuacao
            };

            var produtoComplexidades = produto.ProdutoComplexidades;
            var allComplexidades = complexidadeRepository.GetAll();
            var checkBoxItems = new List<CheckBoxListItem>();

            foreach (var complexidade in allComplexidades)
            {
                checkBoxItems.Add(new CheckBoxListItem() { 
                    Id = complexidade.ComplexidadeId,
                    Display = complexidade.Classificacao,
                    IsChecked = produtoComplexidades.Where(x => x.ComplexidadeId == complexidade.ComplexidadeId).Any()
                });
            }

            produtoView.Complexidades = checkBoxItems;
            return View(produtoView);
        }

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("admin/produto/editar/{id}")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, ProdutoViewModel produtoView)
        {
            var produto = produtoRepository.GetById(id);

            if(ModelState.IsValid && produto != null)
            {
                produto.Nome = produtoView.Nome;
                produto.Esforco = produtoView.Esforco;
                produto.PontoBase = (double) produtoView.Esforco / 8;
                produto.TipoPontuacao = produtoView.TipoPontuacao;

                var complexidadesSelecionadas = produtoView.Complexidades
                  .Where(x => x.IsChecked)
                  .Select(x => x.Id).ToList();

                produtoRepository.Save(produto, complexidadesSelecionadas);
                return RedirectToAction("Index");
            }

            var allComplexidades = complexidadeRepository.GetAll();
            var checkBoxitems = new List<CheckBoxListItem>();

            allComplexidades.
                ForEach(x => checkBoxitems.Add(new CheckBoxListItem()
                {
                    Id = x.ComplexidadeId,
                    Display = x.Classificacao,
                    IsChecked = false
                }));

            produtoView.Complexidades = checkBoxitems;

            ViewBag.Messages = new[] {
                new AlertViewModel("danger", "Algo de errado aconteceu!", "Os dados não foram inseridos corretamente")
            };

            return View(produtoView);
        }

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [Route("admin/produto/excluir/{id}")]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            var produto = produtoRepository.ProdutoExist(id);
            if (!produto)
                return NotFound();

            produtoRepository.Delete(id);
            return RedirectToAction("Index");
        }
    }
}