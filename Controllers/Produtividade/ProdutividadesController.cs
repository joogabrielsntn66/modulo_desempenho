﻿using System.Linq;
using desempenho_cge.Repositories;
using desempenho_cge.ViewModels;
using desempenho_cge.ViewModels.Produtividade;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace desempenho_cge.Controllers
{
    [Authorize]
    public class ProdutividadesController : Controller
    {
        private readonly ProdutividadeRepository repository;
        private readonly ProdutoRepository produtoRepository;
        private readonly AuditorRepository auditorRepository;
        private readonly OrgaoRepository orgaoRepository;

        public ProdutividadesController
            (ProdutividadeRepository _repository,
            ProdutoRepository _produtoRepository,
            AuditorRepository _auditorRepository,
            OrgaoRepository _orgaoRepository)
        {
            repository = _repository;
            produtoRepository = _produtoRepository;
            auditorRepository = _auditorRepository;
            orgaoRepository = _orgaoRepository;
        }

        [HttpGet]
        [Authorize]
        [Route("produtividade/todas")]
        public ActionResult Index() => View(repository.GetAll().AsQueryable());

        [HttpGet]
        [Authorize]
        [Route("produtividade/detalhes/{id}")]
        public ActionResult Details(int id) => View(repository.GetById(id));

        [HttpGet]
        [Authorize]
        [Route("produtividade/criar")]
        public ActionResult Create()
        {
            ViewBag.Auditores = new SelectList(
                auditorRepository.GetAll(), "AuditorId", "Nome");

            ViewBag.Produtos = new SelectList(
                produtoRepository.GetAll(), "ProdutoId", "Nome");
            
            ViewBag.Orgaos = new SelectList(
                orgaoRepository.GetAll(), "OrgaoId", "Nome");
            
            return View();
        }

        [HttpPost]
        [Authorize]
        [Route("produtividade/criar")]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProdutividadeViewModel produtividadeView)
        {
            if(ModelState.IsValid)
            {
                var produtividade = repository.Converter(produtividadeView);
                produtividade.ProdutividadeId = 0;
                repository.Save(produtividade);
                return RedirectToAction("Index");
            }

            ViewBag.Auditores = new SelectList(
                auditorRepository.GetAll(), "AuditorId", "Nome", produtividadeView.AuditorId);

            ViewBag.Produtos = new SelectList(
                produtoRepository.GetAll(), "ProdutoId", "Nome", produtividadeView.ProdutoId);

            ViewBag.Orgaos = new SelectList(
                orgaoRepository.GetAll(), "OrgaoId", "Nome", produtividadeView.OrgaoId);

            ViewBag.Messages = new[] {
                new AlertViewModel("danger", "Algo de errado aconteceu!", "Os dados não foram inseridos corretamente")
            };

            return View(produtividadeView);
        }

        [HttpGet]
        [Authorize]
        [Route("produtividade/editar/{id}")]
        public ActionResult Edit(int id)
        {
            var produtividade = repository.GetById(id);

            if (produtividade == null)
                return NotFound();

            ViewBag.Auditores = new SelectList(
                auditorRepository.GetAll(), "AuditorId", "Nome", produtividade.AuditorId);

            ViewBag.Produtos = new SelectList(
                produtoRepository.GetAll(), "ProdutoId", "Nome", produtividade.ProdutoId);

            ViewBag.Orgaos = new SelectList(
                orgaoRepository.GetAll(), "OrgaoId", "Nome", produtividade.OrgaoId);

            ViewBag.Unidade = produtividade.UnidadeExecucao;
            ViewBag.Complexidade = produtividade.Complexidade;

            return View(repository.Converter(produtividade));
        }

        [HttpPost]
        [Authorize]
        [Route("produtividade/editar/{id}")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, ProdutividadeViewModel produtividadeView)
        {
            var produtividade = repository.GetById(id);

            if(ModelState.IsValid && produtividade != null)
            {
                produtividade = repository.Converter(produtividadeView);
                repository.Save(produtividade);
                return RedirectToAction("Index");
            }

            produtividadeView = repository.Converter(produtividade);

            ViewBag.Auditores = new SelectList(
                auditorRepository.GetAll(), "AuditorId", "Nome", produtividadeView.AuditorId);

            ViewBag.Produtos = new SelectList(
                produtoRepository.GetAll(), "ProdutoId", "Nome", produtividadeView.ProdutoId);

            ViewBag.Orgaos = new SelectList(
                orgaoRepository.GetAll(), "OrgaoId", "Nome", produtividadeView.OrgaoId);

            ViewBag.Unidade = produtividade.UnidadeExecucao;
            ViewBag.Complexidade = produtividade.Complexidade;

            ViewBag.Messages = new[] {
                new AlertViewModel("danger", "Algo de errado aconteceu!", "Os dados não foram inseridos corretamente")
            };

            return View(produtividadeView);
        }

        [HttpPost]
        [Authorize]
        [Route("produtividade/excluir/{id}")]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            var produtividade = repository.ProdutividadeExist(id);
            if (!produtividade)
                return NotFound();
            repository.Delete(id);
            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public JsonResult GetUnidadeFromAuditor(int AuditorId)
        {
            var unidade = auditorRepository
                .GetAll()
                .Where(x => x.AuditorId == AuditorId)
                .Select(x => x.Unidade);

            var unidadeItem = new SelectList(unidade, "Nome", "Nome");

            return Json(unidadeItem);
        }

        [HttpGet]
        public JsonResult GetComplexidadesFromProduto(int ProdutoId)
        {
            var complexidades = produtoRepository
                .GetComplexidadesFromProduto()
                .Where(x => x.ProdutoId == ProdutoId)
                .Select(x => x.Complexidade);
            var complexidadesList = new SelectList(complexidades, "Classificacao", "Classificacao");
            return Json(complexidadesList);
        }

        [HttpGet]
        public JsonResult GetProduto(int ProdutoId)
        {
            var produto = produtoRepository
                .GetAll()
                .Where(x => x.ProdutoId == ProdutoId)
                .Select(x => new { x.TipoPontuacao, x.PontoBase });

            return Json(produto);
        }
    }
}