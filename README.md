# Módulo Desempenho

#### Controle de Produtividade, Capacitação e Qualidade

#### Instruções para rodar:
* Clone o projeto no Visual Studio 2019
  * Necessário ter o SDK .Net Core 3.1 instalado
* Abra o Console do Gerenciador de Pacotes e digite: update-dabase
* Aperte Run
  * Recomendado dar Rebuild antes
